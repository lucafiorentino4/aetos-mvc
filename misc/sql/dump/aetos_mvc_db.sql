-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Erstellungszeit: 07. Nov 2019 um 19:39
-- Server-Version: 5.7.24-log
-- PHP-Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `aetos_mvc_db`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `brand`
--

CREATE TABLE `brand` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `brand`
--

INSERT INTO `brand` (`id`, `name`) VALUES
(9, 'DJI'),
(10, 'Xiaomi'),
(11, 'Go Pro'),
(12, 'Nikon'),
(13, 'Canon'),
(14, 'Leica'),
(15, 'Yuneec'),
(16, 'Parrot');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `camera`
--

CREATE TABLE `camera` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `text` text,
  `brand_id` int(11) DEFAULT NULL,
  `image_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `camera`
--

INSERT INTO `camera` (`id`, `name`, `text`, `brand_id`, `image_id`) VALUES
(45, 'EOS 5D Mark III', '<h3>Canon EOS 5D Mark III SLR-Digitalkamera</h3>\r\n<div>\r\n<h5> </h5>\r\n<p> </p>\r\n<h4>Kreativität neu definiert</h4>\r\nDie EOS 5D Mark III ist Mitglied der weltweit anerkannten Canon EOS DSLR Reihe. Basierend auf der Leistungsstärke der legendären EOS 5D Mark II beeindruckt die 22,3 Megapixel Vollformatkamera mit erhöhter Geschwindigkeit, gesteigerter Auflösung und Prozessorleistung, sowie vielen Kreativ-Optionen für Fotos und Full-HD-Videos.</div>\r\n<div> </div>\r\n<div>\r\n<h4>Der Evolutionsschritt in der EOS 5D Serie</h4>\r\nDie Resonanz von Fotografen aus aller Welt brachte viele entscheidende Hinweise für die Entwicklung der neuen EOS 5D Mark III: Die Kamera zeigt eine gesteigerte Leistung in nahezu jedem Bereich. Der neue Vollformatsensor bietet die ideale Balance aus 22,3 Megapixeln Auflösung für Fotos mit schnellen Reihenaufnahmen von bis zu sechs Bildern pro Sekunde und Full-HD-Videos mit kreativem Spielraum.<br />Das 61-Punkt AF-System und die Belichtungsmessung über 63 Zonen ermöglichen eine Bildqualität auf höchstem Niveau und einen Spitzenlevel an Geschwindigkeit, Flexibilität und Präzision. Die mit DIGIC 5+ Prozessortechnologie ausgestattete EOS 5D Mark III setzt mit innovativen Videofunktionen, exzellenter Bildqualität und umfassender Audiosteuerung einen neuen Standard bei den kreativen Möglichkeiten für Fotografen und auch Hobbyfilmer.<br />Mit der EOS 5D Mark III wird ein bedeutender Evolutionsschritt in der EOS 5D Serie vollzogen. Sie ist eine außergewöhnliche Kamera, deren Entwicklungsschritte auf der Resonanz einer leidenschaftlichen Foto-Community gründen. Das Ergebnis ist eine optimierte Leistung in allen Bereichen. Die Kamera wird nahezu jeder kreativen Herausforderung gerecht: Sie ist schnell, hat ein hervorragendes Ansprechverhalten und Funktionen, die zu allen Anforderungen, von der Studio-Fotografie bis zur kreativen Videogestaltung, passen und Ergebnisse auf höchstem Niveau liefern.</div>\r\n<div> </div>\r\n<div>\r\n<h4>Optimierte Leistung</h4>\r\nDie umfangreichen, technologischen Weiterentwicklungen machen die EOS 5D Mark III zur Empfehlung für die wachsende Zahl an Fotografen, die für Bilder und auch Movies eine hochwertige Kamera benötigen. In der Kamera sind viele Funktionen aus der revolutionären Canon Profi-Kamera EOS-1D X integriert, die außergewöhnliche Leistung, Flexibilität, Anwenderkomfort und Zuverlässigkeit ermöglichen.<br />Der neu entwickelte 22,3 Megapixel Vollformat-CMOS-Sensor sorgt für eine immense Auflösung und feinste Detailzeichnung – beste Voraussetzungen also für viele Motive, von Landschaftsaufnahmen bis zu Porträts. Die schnelle Reihenaufnahmefunktion bietet ausgedehnte kreative Möglichkeiten.<br />Die hohe Auslesegeschwindigkeit über acht Kanäle ist die Basis für schnelle Bildfolgen mit bis zu sechs Bildern pro Sekunde bei voller Auflösung, entweder von bis zu 18 RAW- oder über 16.000 JPEG-Aufnahmen (bei Verwendung einer UDMA 7 kompatiblen Speicherkarte), ohne dass weiteres Zubehör notwendig ist. Die innovative Sensorarchitektur bringt einen riesigen ISO-Bereich von 100 bis 25.6000, erweiterbar auf bis zu ISO 102.4000 – damit ist auch bei extrem wenig Umgebungslicht eine exzellente, detailreiche und rauscharme Bildqualität gewährleistet.<br />Die EOS 5D Mark III arbeitet mit demselben 61-Punkt Weitbereich-Autofokussystem, das auch in dem Flaggschiff der EOS Reihe, der EOS-1D X, integriert ist und für exzellentes Ansprechverhalten, Präzision und Geschwindigkeit steht. Es ist eines der derzeit leistungsfähigsten AF-Systeme und arbeitet mit 41 Kreuzsensoren und fünf Doppel-Kreuzsensoren, die eine präzise Schärfe von Rand zu Rand liefern.<br />Die konfigurierbaren AF-Voreinstellungen die erstmals in der EOS-1D X vorgestellt wurden sind für besonders anspruchsvolle Motive ausgelegt und sorgen für außergewöhnliche Zuverlässigkeit bei traditionell schwierigen Aufnahmesituationen, geben aber auch eine zusätzliche Sicherheit in Situationen, in denen die Motivbewegung nicht vorhersehbar ist.<br />Für präzise belichtete Aufnahmen sorgt das anerkannte Canon iFCL Messsystem mit Dual-Layer-Sensor über 63 Zonen, die mit allen AF-Punkten verbunden sind. Das Belichtungs-Messsystem nutzt die Farb- und Helligkeitsinformationen, die vom Sensor gemessen und von den Fokus-Messfeldern übermittelt werden.<br />Im Ergebnis entstehen hervorragend belichtete Aufnahmen mit präzisen Hauttönen und von exzellenter Qualität in den unterschiedlichsten Aufnahmesituationen.</div>', 13, '137'),
(46, 'ION L1 Pro', '<h2><strong>Neue Maßstäbe in der Luftbildfotografie</strong></h2>\r\n<p><br />Benutzerdefinierte Bildmodus- und Schärfeeinstellungen, der verlustfreie Digitalzoom und die überragende Bildqualität machen die ION L1 Pro in ihrer Klasse zur Kamera der Wahl in allen Bereichen der Luftbildfotografie. Im Einklang mit Leicas Leitmotiv \"Das Wesentliche\" wurden die Funktionen auf das Nützliche und Mehrwertschaffende ausgerichtet. Die Software funktioniert jetzt so, wie es ein Fotograf gewohnt ist. Halbautomatische Modi, ISO-Werte oder Belichtungsintervalle wurden zum Beispiel denen einer normalen Kamera nachempfunden. Dank Cruise Control hält die Drohne konstant ihre Geschwindigkeit und Sie können sich ganz auf Ihre Kameraaufnahme konzentrieren. Mit dem integrierten 3-Achs-Gimbal und dem einziehbaren Landegestell der Drohne lässt sich die Kamera unbegrenzt um 360° drehen und ermöglicht die Aufnahme von fließenden Panoramaaufnahmen.</p>\r\n<h3><br /><strong>Herausragende Bildqualität</strong></h3>\r\n<p><br />Die Komponenten des H3 und der L1 Pro sind präzise aufeinander abgestimmt und ermöglichen die Aufnahme von detailreichen und brillanten Bildern aus der Luft im legendären Leica Look. Dank aufwendiger Untersuchungen und entsprechend wirksamer Streulichtunterdrückung, liefert die Kamera, gerade auch in kritischen Licht-Situationen, bestmögliche Bildergebnisse. Die technische Vielseitigkeit der Kamera bietet Ihnen auf Ihrer fotografischen Reise alle Möglichkeiten - von Belichtungsreihen- und Halbautomatik bis zu Wide Dynamic Range. Gemeinsam mit Leica haben wir Y-Log zur professionellen und einfachen Integration von Videomaterial in Projekte mit anderen Bildquellen implementiert. Das Y-Log-Videomaterial enthält eine deutlich bessere Dynamik als Standardvideos. Leica hat auf Basis eines ganzheitlichen Bildoptimierungsansatzes hochwertige ADOBE DNG-Profile erstellt - unterstützt ab Release 11.3 der Camera Raw-Software. Die Profile beinhalten Vignettierungskorrektur, Verzeichnungskorrektur und Korrektur von chromatischen Aberrationen. Zudem wurde die Farbwiedergabe \"eingemessen\" und ist nun bei DNGs äußerst präzise. Das DNG-Profil wird automatisch ausgewählt. Sie müssen lediglich die neueste Version von Camera RAW installieren.</p>', 14, '138'),
(47, 'D5', '<h2>Die D5 von Nikon - DSLR Kamera für Profifotografen</h2>\r\n<p>Als Profi-Modell im Kameraprogramm von Nikon bietet die D5 wesentliche Verbesserungen gegenüber ihrer Vorgängerin, der D4S. Diese umfassen eine noch höhere Bildqualität und eine nochmals verbesserte Autofokusleistung.<br /><br />Die Nikon D5 ermöglicht ambitionierten Fotografen die Aufnahme phänomenaler Bilder und bewältigt so auch die größten fotografischen Herausforderungen. Ihr AF-System der nächsten Generation mit 153 Messfeldern sorgt für eine sehr große Bildfeldabdeckung, ob bei Sportveranstaltungen oder am roten Teppich. Der größte Empfindlichkeitsbereich in der Geschichte von Nikon macht es tatsächlich möglich, Dinge zu fotografieren, die für das bloße Auge nicht erkennbar sind. Filmemacher, die das Besondere schätzen, können Videos in 4K UHD direkt in der Kamera aufnehmen.<br /><br />Die D5 bietet ganz neue Möglichkeiten der Fokuserfassung und ist strapazierfähiger als ihre Vorgänger. Wenn Du beim Fotografieren in extreme Aufnahmesituationen geräst, in denen Du nur eine Chance für das perfekte Bild bekommst, lässt Dich diese Kamera nicht im Stich. Die effektive Auflösung der D5 ist hervorragend, und Profis, die sie in unterschiedlichen Bereichen einsetzen, profitieren von den Ergonomie- und Workflow-Verbesserungen der Kamera. Diese basieren auf dem Feedback der Fotografen und reduzieren den Stress, den die professionelle Fotografie mit sich bringt, deutlich.</p>', 12, '139'),
(48, 'Xenmuse XT2', '<h3>Duale Sicht. Überlegene Intelligenz.</h3>\r\n<p>Erlebe die nächste Generation kommerzieller Drohnenlösungen mit der XT2.Durch Kombination des FLIR Tau 2 Temperatursensors, einer 4K Sichtkamera,führender Stabilisierungstechnik und künstlicher Intelligenz von DJI, erstelltdie XT2 aus Luftbildaufnahmen aussagekräftige Informationen, die Zeit, Geldund Leben retten können.</p>\r\n<h2>Robuste Wärmebild-Lösung mit dualen Sensoren</h2>\r\n<p>Der moderne radiometrische FLIR Temperatursensor ist zusammen mit dem 4K Sichtsensor in einemwetterfesten Gehäuse untergebracht, nahtlos integriert mit den Enterprise-Drohnen von DJI.</p>\r\n<p> </p>\r\n<h2>Verwertbare Informationen in Echtzeit erhalten</h2>\r\n<p>Durch eine Vielzahl an Prozessoren und Bilderkennungsalgorithmen hilft die virtuelle Intelligenz der XT2den Teams vor Ort sofort bei der Filterung von Informationen, statt in langwierigen Analysen im Nachhinein.</p>\r\n<h2>Anwendungen</h2>\r\n<p>Entdecke das Unsichtbare. FLIR Kameras zeigen kleinste Temperaturunterschiede, die für das bloße Auge nicht sichtbar sind.Diese neue Sicht auf die Welt hilft zu erkennen, welche Einrichtungen oder Gebäude beschädigt sind,findet vermisste Personen und kann darüber hinaus noch viel mehr.</p>', 9, '140'),
(49, 'E90', '<h2>Ultra High Definition &amp; Digital Zoom</h2>\r\n<div>\r\n<p>The E90 camera is a wide-angle, high resolution, gimbal stabilized imaging system perfectly suited for use in applications that require high quality photo and video. The E90 utilizes a 20 MP 1-inch sensor and the latest H2 high speed image processing chip. The digital zoom feature allows the user to digitally zoom in and out with two finger gesture. Zoom factor is up to 8x, whereas the view on the ST16S is lossless up to.</p>\r\n<h2>Hot-swap capable</h2>\r\n<div>\r\n<p>The E90 is hot-swap capable which minimizes down time and improves productivity. The E90 camera-gimbal combination may be swapped for an <a href=\"https://www.yuneec.com/en_GB/accessories/cameras/e50/overview.html\">E50</a> or <a href=\"https://www.yuneec.com/en_GB/accessories/cameras/cgo-et/overview.html\">CGOET</a> camera-gimbal combo without power cycling the airframe. The quick release and lock mechanism of the gimbal and airframe mounting system enables fast and efficient exchange of cameras.</p>\r\n</div>\r\n</div>', 15, '141'),
(50, 'E50', '<h2>Big lense quality in a compact size</h2>\r\n<div>\r\n<p>The E50 camera is a medium focal length, high resolution, gimbal stabilized imaging system perfectly suited for use in inspection or cinema applications. The E50 utilizes a high aperture 1/2.3 inch CMOS imager that is capable to capture still images with 12 MP resolution. </p>\r\n<h2>Hot-swap capable</h2>\r\n<div>\r\n<p>The E50 is hot-swap capable which minimizes down time and improves productivity. The E50 camera-gimbal combination may be swapped for an <a href=\"https://www.yuneec.com/en_GB/accessories/cameras/e90/overview.html\">E90</a> or <a href=\"https://www.yuneec.com/en_GB/accessories/cameras/cgo-et/overview.html\">CGOET</a> camera-gimbal combo without power cycling the airframe. The quick release and lock mechanism of the gimbal and airframe mounting system enables fast. </p>\r\n</div>\r\n</div>', 15, '142'),
(51, 'E10T', '<h2>Hochauflösende DuHochauflösende Dualkamera</h2>\r\n<p><br />Die E10T mit FLIR® Boson Sensor für den H520 Hexacopter ist stabilisiertes 3-Achs-Gimbal, Wärmebild- und Restlichtkamera zugleich. Die E10T ist in zwei verschiedenen Auflösungsvarianten verfügbar: Mit einer Auflösung von 320 x 256 Pixeln (E10T) oder 640 x 512 Pixeln (E10Tv) nimmt die E10T hochwertige Thermalaufnahmen auf und erkennt dank des großen RGB-Sensors im Dunkeln mehr Details als das menschliche Auge. Dank des dualen Videostreams können Sie das Wärme- und Restlichtbild gleichzeitig auf die Fernsteuerung streamen und das Bild dabei als Overlay oder Bild-in-Bild betrachten. Darüber hinaus unterstützt die E10T Commercial Applikationen wie beispielsweise Mission Planning.<br /><br /></p>\r\n<h3><br />E10T mit H520 Hexacopter</h3>\r\n<p><br />Der H520 Hexacopter wurde speziell für Inspektions-, Sicherheits- und Search &amp; Rescue-Anwendungen entwickelt. Mit mehrfacher Sensorkompatibilität bietet der H520 Hexacopter in Kombination mit der E10T Wärmebild- und Restlichtkamera Rettungsdiensten, Feuerwehrleuten, Polizei und Inspekteuren eine zuverlässige und effiziente Luftunterstützung.<br /><br /><br /></p>\r\n<h3><br />Wärmebildanwendungen</h3>\r\n<p><br />Bei Personensuchen befähigt der H520 mit E10T Wärmebildkamera Sie Gebiete aus der Luft in kürzester Zeit nach den Vermissten abzusuchen. Bei Bränden lassen sich Feuer Hot-Spots erkennen und ermöglichen ein effektives und schnelles Vorgehen gegen das Feuer, das in manchen Fällen Leben retten kann. Zudem lassen sich mit dem H520 alle nötigen Details eines Inspektionsobjektes erkennen, ohne dem Gegenstand dabei zu nahe zu kommen. Während des Fluges lässt sich das Livebild der Drohne auf einen Monitor übertragen, sodass mehrere Leute an der Inspektion teilhaben und schon während des Flugs erste Analysen<br /><br /></p>\r\n<h3><br />3-Achs-Gimbal mit 360° Rotation</h3>\r\n<p><br />Die E10T Kamera/Gimbal-Kombination mit FLIR® Boson Sensor wird über drei Achsen stabilisiert und sorgt somit für ein ruhiges Untersuchungsbild und stabile Aufnahmen. Darüber hinaus lässt sich das Gimbal um 360° rotieren und sorgt zusammen mit dem einziehbaren Landegestell des H520 für eine volle Rundumsicht. treffen können.<br /><br /><br /></p>\r\n<h3><br />Austauschbare Kamerasysteme</h3>\r\n<p><br />Die E10T ist hot-swap-fähig und kann gegen eine beliebige andere mit dem H520 kompatible Kamera/Gimbal-Kombination wie beispielsweise der E90 1“-Sensor-Kamera oder der E50 Inspektionskamera getauscht werden. Damit Sie stets produktiv und effektiv arbeiten können, muss der Copter bei einem Austausch der Kamerasysteme nicht neu gestartet werden und erspart Ihnen erzwungene Pausen.</p>\r\n<p>Die E10T mit FLIR® Boson Sensor für den H520 Hexacopter ist stabilisiertes 3-Achs-Gimbal, Wärmebild- und Restlichtkamera zugleich. Die E10T ist in zwei verschiedenen Auflösungsvarianten verfügbar: Mit einer Auflösung von 320 x 256 Pixeln (E10T) oder 640 x 512 Pixeln (E10Tv) nimmt die E10T hochwertige Thermalaufnahmen auf und erkennt dank des großen RGB-Sensors im Dunkeln mehr Details als das menschliche Auge. Dank des dualen Videostreams können Sie das Wärme- und Restlichtbild gleichzeitig auf die Fernsteuerung streamen und das Bild dabei als Overlay oder Bild-in-Bild betrachten. Darüber hinaus unterstützt die E10T Commercial Applikationen wie beispielsweise Mission Planning.</p>', 15, '159'),
(52, 'Zenmuse X5S', '<h2>ZENMUSE X5S</h2>\r\n<p><br />Ausgestattet mit einem verbesserten Micro 4/3 Sensor, verfügt die Zenmuse X5S über einen Dynamikbereich von 12,8 Blendstufen und einen stark verbesserten Rauschabstand mit besserer Farbempfindlichkeit im Vergleich zur X5R-Kamera. Die X5S-Kamera unterstützt bis zu acht Standard M4/3 Objektive (und Zoom-Objektive) mit einer Brennweite von 9-45 mm (äquivalent zu 18-19 mm auf einer 35 mm Kamera), für maximale kreative Freiheit. Das neue CineCore 2.0 Bildverarbeitungssystem des Inspire 2 ermöglicht der Zenmuse X5S Aufnahmen in 5,2K CinemaDNG und Apple ProRes bei 30 Bildern die Sekunde, sowie Aufnahmen mit H.264 und H.265, bei jeweils 4K/60 beziehungsweise 4K/30, bei bis zu 100 Mbit/s. Kontinuierliche Serienbilder können in DNG bei 20 Bildern die Sekunde mit 20,8 MP aufgenommen werden.<br />Die Zenmuse X5S wurde konzipiert um mit den rigorosen Anforderungen der High-End Luftbildfotografie Schritt zu halten.</p>\r\n<h3><br />INSPIRE 2 LUFTBILDFOTOGRAFIE</h3>\r\n<p><br />Im Vergleich zum Inspire 1, nutzt der Inspire 2 eine neue innovative Struktur, welche das CineCore 2.0 Bildverarbeitungssystem von der Kamera in die Drohne verlagert. Das bedeutet, dass der Sensor und das optische System komplett separat fungieren und ganz einfach durch eine neue Schnellentriegelung in den Inspire 2 eingesetzt werden können. Das Kamerasystem auf diese Weise zu konzipieren, hat den Vorteil, dass der Sensor vor magnetischen Interferenzen durch den Prozessor geschützt ist. Es erlaubt darüber hinaus eine großen Anzahl an Kameras, passend für die entsprechende Szene, in wenigen Sekunden austauschen zu können. Der verbesserte M4/3 Sensor unterstützt austauschbare Objektive und erlaubt der Zenmuse X5S hochauflösende Bilder in unglaublicher Qualität aufzunehmen, wenn dieser mit dem außerordentlichen CineCore 2.0 Bilderverarbeitungssystem genutzt wird.</p>', 9, '160');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `camera_category`
--

CREATE TABLE `camera_category` (
  `id` int(11) NOT NULL,
  `camera_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `camera_category`
--

INSERT INTO `camera_category` (`id`, `camera_id`, `category_id`) VALUES
(87, 45, 29),
(88, 45, 33),
(89, 45, 40),
(90, 45, 42),
(91, 46, 29),
(92, 46, 31),
(93, 46, 33),
(94, 46, 35),
(95, 46, 40),
(96, 47, 29),
(97, 47, 35),
(98, 47, 42),
(99, 48, 31),
(100, 48, 36),
(101, 48, 37),
(102, 49, 29),
(103, 49, 31),
(104, 49, 33),
(105, 49, 35),
(106, 49, 40),
(107, 49, 42),
(113, 50, 29),
(114, 50, 31),
(115, 50, 33),
(116, 50, 40),
(117, 50, 42),
(118, 51, 31),
(119, 51, 36),
(120, 51, 37),
(121, 52, 29),
(122, 52, 31),
(123, 52, 33),
(124, 52, 35),
(125, 52, 40),
(126, 52, 42);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(29, 'Professionelle Filmproduktionen'),
(30, 'Hobby Drohnen'),
(31, 'Arbeitsabläufe in der Luft'),
(32, 'Mini Drohnen'),
(33, 'Professionelle Luftaufnahmen'),
(34, 'Action Cam'),
(35, '4K'),
(36, 'Luftthermografie'),
(37, 'Thermal Kamera'),
(38, 'Sprachsteuerung'),
(39, 'Faltbare Reisedrohne'),
(40, 'Luftbildfotografie'),
(41, 'Intelligente Flugmodi'),
(42, 'Fotografie');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `drone`
--

CREATE TABLE `drone` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `text` text,
  `brand_id` int(11) DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `drone`
--

INSERT INTO `drone` (`id`, `name`, `text`, `brand_id`, `image_id`) VALUES
(189, 'Mavic Pro', '<h3><strong>MAVIC PRO WOHIN DU AUCH GEHST</strong></h3>\r\n<p><br />Die Mavic Pro ist eine kleine und gleichzeitig unfassbar leistungsfähige Drohne von DJI. Sie verwandelt den Himmel in Deine Spielwiese und hilft Dir jeden Moment spielend einfach in luftigen Höhen festzuhalten. Die kompakte Größe und der hohe Grad an Komplexität machen sie zu einer der fortschrittlichsten fliegenden Kameras, die DJI jemals entwickelt hat. 24 Hochleistungs-Prozessoren, ein komplett neues Übertragungssystem mit 7 km Reichweite, 4 Sichtsensoren und eine 4K Kamera, stabilisiert durch einen mechanischen 3-Achsen Gimbal, stehen Dir per Knopfdruck zur Verfügung.*ungehinderte Sicht, frei von Interferenzen, FCC konform.</p>', 9, 143),
(190, 'Phantom 4 Pro Obsidian', '<h3>Leisere und angenehmere Flüge</h3>\r\n<p><br />Neue aerodynamische 9455S Propeller und neue FOC ESC-Einheiten bieten einen reduzierten Geräuschpegel und mehr Stabilität.Der Geräuschpegel des Fluggeräts wurde um bis zu 4 dB* (60%) reduziert.</p>\r\n<h3><br />Neues Ocusync-übertraguns-System</h3>\r\n<p><br />Die Fernsteuerung der Phantom 4 Pro V2.0 verwendet das OcuSync-Videoübertragungsmodul*, welches simultan Steuerungssignale senden und Videosignale erhalten kann. OcuSync sucht automatisch nach dem Frequenzband mit den wenigsten Interferenzen und wechselt dieses automatisch, um stets für klare Übertragungen zu sorgen. Dank OcuSync ist die Phantom 4 Pro V2.0 jetzt in der Lage sich kabellos mit den DJI Goggles RE zu verbinden. Genieße aufregende FPV-Flüge mit bis zu 72 km/h im Sportmodus!</p>\r\n<h3><br />Dual-Frequenz-Übertragung</h3>\r\n<p><br />Die Fernsteuerung der Phantom 4 Pro V2.0 nutzt das OcuSync-Videoübertragungssystem, welches die Frequenzbänder 2,4 und 5,8 GHz verwendet und damit eine maximale Übertragungsreichweite von 7 km erlaubt. Das Gerät analysiert die Umgebungsbedingungen und wählt automatisch die beste Frequenz, um Interferenzen zu reduzieren.</p>\r\n<h3><br />Schneller Frequenzwechsel</h3>\r\n<p><br />OcuSync ist in der Lage Umgebungsinterferenzen in Echtzeit zu erkennen. Bei starken Interferenzen wechselt das System automatisch auf das beste Frequenzband.</p>', 9, 144),
(191, 'Karma', '<h2>GoPro Karma Drohne</h2>\r\n<p><br />Karma captures amazingly smooth GoPro footage in the air, handheld or body mounted. Compact and ultra portable, the Karma Drone folds to fit into its own lightweight case. The game-style controller with an integrated touch display makes Karma easy and fun to fly. The included camera stabilizer can be removed from the drone and attached to the included Karma Grip for ultra steady professional-looking handheld and body-mounted footage. More than a drone. It’s Karma.<br /><br />The Karma Drone folds to fit into the included lightweight case. Take it anywhere. When you’re ready to fly, just unfold, attach the propellers and go.<br />The Karma Stabilizer captures breathtaking shake-free video both in the air and on the ground.<br />Remove the Karma Stabilizer and attach it to Karma Grip for ultra steady, professional-looking handheld shots.<br />Karma Grip works with GoPro body mounts, including the built-in shoulder mount on the Karma Case, to deliver ultra steady body-mounted footage.<br />Flying is fun with a game-style controller that makes it easy to fly the drone1 and control your camera at the same time.<br />The Karma Controller’s built-in touch display streams a live, first-person view of what Karma sees.<br />The controller’s touch display was specially designed for improved visibility in bright outdoor conditions.<br />The built-in tutorial and flight simulator make it easy and fun to learn how to fly anywhere, anytime before your first flight.<br />Automatic one-button takeoff and landing helps first-time pilots fly with confidence.<br />Built-in No-Fly Zones prevent the drone from accidentally flying into restricted areas.<br />Karma safely flies back to you when out of range, if there’s a lost connection or when the battery runs low.<br />Designed from the ground up to give you full control of your GoPro in flight, Karma lets you remotely control all camera modes and capture settings, add HiLight Tags and more using its built-in touch display.<br />The GoPro Passenger™ App lets friends view your flight and control your GoPro while you fly.<br />Pro-quality shots are easy to capture on your first attempt using Auto Shot Paths to capture professional-style drone selfies (Dronies), Cable Cam, Orbit and Reveal shots.<br />Arms, landing gear and other components are easily replaceable. Extra propellers are included.</p>', 11, 145),
(192, 'Spark', '<h1>SPARK</h1>\r\n<p>DEIN AUGENBLICK. DEINE ERINNERUNG.</p>\r\n<p>Die DJI Spark ist ein Mini-Drohne, die alle fortschrittlichen Technologien von DJI in sich vereint. Fange Deine ganz besonderen Erinnerungen im exakt richtigen Augenblick ein. Mit intelligenten Flugfunktionen, innovativer Steuerung, einem mechanischen Gimbal und einer hochwertigen Kamera, sind Deiner Kreativität keine Grenzen gesetzt.</p>', 9, 146),
(193, 'Mavic 2 PRO', '<h1>MAVIC 2 PRO</h1>\r\n<h5>See the Bigger Picture</h5>\r\n<p>Wir wollten schon immer eine Drohne konzipieren, die moderne Technik, ausgefeilte Ingenieurskunst und die Bedürfnisse von Luftbildfotografen miteinander verbindet. Eine Drohne, die alle bewährten Technologien von DJI in sich vereint und völlig neu definiert, was in der Welt der Luftbildfotografie möglich ist. Mit der DJI Mavic 2 haben wir diesen Traum nun endlich verwirklicht.</p>\r\n<h2>Mavic 2 Pro<br />Mit Hasselblad-Kamera</h2>\r\n<p>Die Kameras des schwedischen Herstellers Hasselblad stehen als Symbol für ergonomisches Design, kompromisslose Bildqualität und schwedische Handwerkskunst. Seit 1941 haben Kameras von Hasselblad einige der bewegendsten Momente in einem Bild eingefangen, wie beispielsweise die erste Mondlandung.<br />Entwickelt in zweijähriger Zusammenarbeit zwischen DJI und Hasselblad, konnten wir die Mavic 2 Pro mit der Hasselblad L1D-20c ausstatten. Zudem hilft die Hasselblad Natural Colour Solution (HNCS)5 Technologie bei der Aufnahme großartiger Luftaufnahmen mit atemberaubender Detailgenauigkeit und Farben, bei einer Auflösung von 20 Megapixeln.</p>', 9, 149),
(194, 'Tornado H920 Plus', '<h3>Die Profi-Drohne von YUNEEC</h3>\r\n<p><br />Der Copter Tornado H920 Plus von Yuneec ist eine Multirotor-Plattform für Fotografie und Videografie aus der Luft. Das einzigartige Design der Profi-Drohne und die innovativen Funktionen ermöglichen Profis in einer Vielzahl von Anwendungsbereichen großartige Fotos und Videoaufnahmen mit dem hochwertigen Flugroboter.</p>\r\n<h2> </h2>\r\n<h2>Quick-Release-Propeller</h2>\r\n<p><br />Um noch schneller einsatzbereit zu sein, ist der Mulicopter Tornado H920 Plus serienmäßig mit den praktischen Quick-Release-Propellern ausgestattet, sodass der Einsatz eines Schraubendrehers bei Montage und Demontage entfällt. Die handlaminierten Carbon-Propeller werden nun nur noch mit einer Vierteldrehung eingeklinkt und können per Knopfdruck genauso schnell wieder von der Profi-Drohne gelöst werden.</p>', 15, 150),
(195, 'Mavic Air', '<div>\r\n<h1>MAVIC AIR</h1>\r\n<h2>Entfalte Deine Abenteuerlust</h2>\r\n<p>Die Mavic Air ist ein Meisterwerk der Ingenieurskunst und ein treuer Begleiter auf jedem Abenteuer.Die kompakte Drohne übernimmt die besten Eigenschaften der Mavic Serie und bietet bahnbrechende Leistungund Funktionalität für den Abenteurer in jedem von uns.</p>\r\n<h2>3-Achsen Gimbal</h2>\r\n<p>Die Mavic Air ist die bis dato kompakteste Drohne von DJI, die über einen extrem präzisen mechanischen 3-Achsen Gimbal verfügt. Die dreieckige Dämpferformation des Gimbals bringt zusätzliche Stabilität in jede Aufnahmen.</p>\r\n</div>', 9, 151),
(196, 'Mi Drone', '<h3>Relax and enjoy the ride</h3>\r\n<p>Our controls are so simple that it is easy for beginners to pick up in a snap. With just one button on our Mi Drone<br />remote control or app, send the drone to flight, land, return, or even follow a desired route. You can even use the app<br />to set it circling around an object. Now with Mi Drone, you can take flight and marvel at the wonders of nature from a<br />different perspective.</p>\r\n<h3><br />Circle around an object</h3>\r\n<p><br />Choose a point and set desired hover height and radius. Mi Drone will automatically circle around an object,<br />capturing beautiful shots of the object.</p>\r\n<h3><br />Immerse yourself in full</h3>\r\n<p><br />HD video transmission<br />Mi Drone (4K version) has low latency and strong anti-inteference<br />technology built in so you can enjoy a bird\'s eye view, even<br />when you are 2km away.</p>\r\n<h3><br />Long battery life, captures as many perfect moments as possible</h3>\r\n<p><br />Comes with 5100mAh high energy density LG/Grepow batteries, so flight time can be as long as 27 mins. It is also simple and easy to<br />replace with new batteries, and with intelligent protection technology makes batteries safe and lasts longer.<br /><br /></p>', 10, 152),
(197, 'Inspire', '<h3>GRENZENLOSE KREATIVITÄT</h3>\r\n<p><br />Die fortschrittlichste Technologie von DJI, verbunden mit einer bedienerfreundlichen Steuerung, lässt Dich mit dieser sofort einsatzbereiten Flugplattform, unvergleichliche Aufnahmen erschaffen.</p>', 9, 153),
(198, 'Phantom 4 Pro', '<h3>DJI Phantom 4 Pro</h3>\r\n<p>Die verbesserte Kamera ist mit einem 1-Zoll 20 Megapixel Sensor ausgestattet, welcher in der Lage ist Aufnahmen in 4K bei 60 Bildern die Sekunde aufzunehmen, sowie Serienaufnahmen mit 14 Bildern die Sekunde. Durch die Nutzung einer Titanlegierung und Magnesium, konnte die gesamte Konstruktion der Drohne noch stabiler und leichter gestaltet werden, was dem Phantom 4 Pro ein ähnliches Gewicht wie dem Phantom 4 gibt, bei mehr Funktionen. Das FlightAutonomy-System erhielt zusätzlich duale nach hinten gerichtete Sensoren und zwei Infrarot-Sensoren, was eine Hinderniserkennung in fünf Richtungen erlaubt und darüber hinaus Hindernisvermeidung in 4 Richtungen.</p>\r\n<h5>Kamera mit 1-Zoll 20 MP Sensor</h5>\r\n<p>Die integrierte Kamera wurde neu konzipiert um einen 1-Zoll 20 Megapixel CMOS-Sensor zu nutzen. Ein speziell entworfenes Objektiv aus 8 Elementen, wurde in sieben Gruppen unterteilt. Es ist die erste DJI Kamera, welche eine mechanische Blende nutzt, die Rolling-Shutter-Verzerrungen eliminiert, die bei Bildern unter hoher Geschwindigkeit auftreten. Dies macht die Kamera ebenbürtig mit vielen traditionellen Bodenkameras. Leistungsfähigere Videoverarbeitung unterstützt H.264 Videos in 4K bei 60 Bildern die Sekunde, oder H.265 Videos in 4K bei 30 Bildern die Sekunde, bei jeweils 100 Mbit/s. Fortschrittliche Sensoren und Prozessoren sichern Aufnahmen mit mehr Details und den notwendigen Daten, die für fortschrittliche Nachbearbeitung benötigt werden.</p>\r\n<h5>4K optimiert für professionelle Produktionen</h5>\r\n<p>Ein verbessertes Videoverarbeitungssystem erlaubt Videos in kinoreifer und für Produktionen optimierten DCI 4K/60 (4096 x 2160/60fps) aufgenommen zu werden, bei einer Bitrate von 100 Mbit/s. Für Aufnahmen in hoher Auflösung und in Zeitlupe. Der Phantom 4 Pro unterstützt den H.265 Video-Codec (maximale Auflösung 4096X2160/30fps). Bei entsprechender Bitrate, verdoppelt der H.265 Codec die Menge der Bildverarbeitungsprozesse gegenüber dem H.264 Codec, was zu einer signifikante Verbesserung der Bildqualität führt. Nimm im D-log Modus mit hohem Dynamikbereich auf und hole das Beste aus den Bildern heraus, für beste Farbkorrektur.</p>', 9, 154),
(199, 'Matrice 100', '<h2>MATRICE 100</h2>\r\n<h3>QUADKOPTER FÜR ENTWICKLER</h3>\r\n<div><a href=\"https://www.djivideos.com/video_play/3b7b0411-37ff-4323-b4a5-04756db05b8b\" target=\"_blank\" rel=\"noopener\">PROMO VIDEO</a>  |  <a href=\"https://www.djivideos.com/video_play/4563fca4-34bf-4acc-8596-8c026559873e\" target=\"_blank\" rel=\"noopener\">UNBOXING VIDEO</a></div>\r\n<p>Was bringt die Zukunft des Fliegens? Jetzt kannst du die Antwort kreieren.Ob für die Forschung, für Business oder zum Spaß verwendet, ist der Matrice 100 eine stabile, flexible und leistungsstarke Plattform, die deine Träume Wahrheit werden läßt.</p>\r\n<h2>KOMPLETTE FLUG-PLATTFORM</h2>\r\n<p>Starte durch und genieße einen stabilen, zuverlässigen Flug. Deinem Matrice 100 ist die Einfach-zu-Fliegen-Technologie von DJI eingebaut. Er kommt mit Flugkontroller, Propulsionssystem, GPS, DJI Lightbridge, einer speziellen Fernbedienung und einem wiederaufladbaren Akku. Das System verwaltet die kompliziertesten Flugmanöver, sodass Du dich auf deine Areit konzentrieren kannst.</p>\r\n<h2>VOLL PROGRAMMIERBAR</h2>\r\n<div>\r\n<p>Passe die Flugplattform komplett deinen Bedürfnissen an mit dem DJI SDK. Entwickle ein System für irgendeinen Gebrauch und revolutioniere ganze Industrieren, indem Du deine Kenntnisse und Fähigkeiten auf modernste Flugtechnik anwendest.</p>\r\n<p>Lerne deinem Matrice 100 wie er sich verhalten soll. Programmiere anpassbare Befehle und zeig ihm, wie und wohin er fliegen soll. Dabei kannst Du Daten vom ganzen System in Echtzeit sammeln.</p>\r\n<h2>LANGE, ERWEITERBARE FLUGZEIT</h2>\r\n<div>\r\n<p>Ein leichter Rahmen und effiziente Motoren ermöglichen dir bis zu 40 Minuten* Flugzeit. Um auch bei zusätzlicher Nutzlast Flexibilität zu gewährleisten, kann eine optionale zweite Batterie in einem Power-Slot angebracht werden.</p>\r\n</div>\r\n</div>', 9, 155),
(200, 'Mavic Pro Platinum', '<p>MAVICPRO</p>\r\n<p>PLATINUM</p>\r\n<p>Die Mavic Pro Platinum verfügt über ein schnittiges und kompaktes Design. Die verbesserte Flugzeit von bis zu 30 Minuten und eine Geräuschreduktion von 60%, machen die Mavic Pro Platinum zu einer der raffiniertesten und ausgeklügelsten Drohnen von DJI.</p>\r\n<p>Verbesserte Flugzeit,leise in der Luft</p>\r\n<p>Neue sinusförmige Drehzahlregler (ESC) und Propeller vom Typ 8331 reduzieren den Geräuschpegel während des Fluges um bis zu 4 dB* (60%) und verlängern darüber hinaus die Flugzeit auf bis zu 30 Minuten.</p>\r\n<div class=\"mavic-s4 introduce clearfix\" style=\"box-sizing: border-box; margin: 51px 0px 0px; padding: 0px; border: 0px; outline: 0px; font-family: \'Open Sans\', \'PingFang SC\', \'Microsoft YaHei\', \'Helvetica Neue\', \'Hiragino Sans GB\', \'WenQuanYi Micro Hei\', Arial, sans-serif; vertical-align: baseline; width: 575px; color: #707473; font-size: 14px; word-spacing: -1px; background-color: #ffffff;\">\r\n<div class=\"s4-left pull-left\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; float: left;\">\r\n<p class=\"font-opensans s4-left-num pull-left\" style=\"box-sizing: border-box; margin: -41px 0px 0px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; vertical-align: baseline; float: left; font-size: 112px; color: #303233; display: inline-block;\">4</p>\r\n<p class=\"s4-left-arrows pull-left\" style=\"box-sizing: border-box; margin: 0px 0px 0px 5px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; float: left; width: 12px; height: 81px; background-size: cover; display: inline-block; background-image: url(\'//www5.djicdn.com/assets/images/products/mavicpt/greenarrows-992d7ef68da619d41b90f0beb513286f.png\');\"> </p>\r\n<p class=\"s4-left-font pull-left\" style=\"box-sizing: border-box; margin: 0px 0px 0px 10px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; float: left; width: 123px; font-size: 16px; color: #303233; display: inline-block;\">dB*<br style=\"box-sizing: border-box;\" />Geräusch-reduktion</p>\r\n</div>\r\n<div class=\"s4-right pull-right\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; float: right;\">\r\n<p class=\"font-opensans s4-left-num pull-left\" style=\"box-sizing: border-box; margin: -41px 0px 0px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; vertical-align: baseline; float: left; font-size: 112px; color: #303233; display: inline-block;\">30</p>\r\n<p class=\"s4-left-arrows pull-left\" style=\"box-sizing: border-box; margin: 0px 0px 0px 5px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; float: left; width: 12px; height: 81px; background-size: cover; display: inline-block; background-image: url(\'//www5.djicdn.com/assets/images/products/mavicpt/bluearrows-e88593ac3407795654c8f8792bf0a3ef.png\');\"> </p>\r\n<p class=\"s4-left-font pull-left\" style=\"box-sizing: border-box; margin: 0px 0px 0px 10px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; float: left; width: 123px; font-size: 16px; color: #303233; display: inline-block;\">MinMax. Flugzeit</p>\r\n</div>\r\n</div>', 9, 156),
(201, 'Mantis q', '<h3>Travel mate</h3>\r\n<p><br />Mantis Q – die faltbare und unkomplizierte Reisedrohne für kleine und große Abenteuer. Dank Sprachsteuerung hört sie aufs Wort und mit dem Visual Tracking folgt sie dir wohin du willst. Ob beim Backpacking in Thailand, dem Roadtrip durch Südfrankreich oder mit deiner Familie am Strand – Mantis Q ist dank seines energieeffizienten Designs für bis zu 33 Minuten in der Luft und verschafft dir ungewöhnlich Fotomotive und Videoclips von oben.<br /><br /><br /></p>\r\n<h3><br />Drohne mit Sprachsteuerung</h3>\r\n<p><br />\"Wach auf\" - Der Mantis Q ist die erste Kameradrohne von YUNEEC, die intuitiv per Sprachbefehl gesteuert werden kann. Egal ob beim Starten, Landen oder Fotos und Videos aufnehmen: Mit Kommandos wie \"Komm zurück\" und \"Aufnahme starten\" lässt sich der Mantis Q einfach via Sprachbefehlen steuern. Eine Auflistung der deutschen Sprachbefehle finden Sie in den Einstellungen der Yuneec Pilot App mit Klick auf das \"i\" neben dem Menü-Punkt \"Sprachsteuerung\". Die Drohne, die auf\'s Wort hört! </p>\r\n<h3><br />Social Media Anbindung</h3>\r\n<p><br />Egal ob du im Camper durch die Welt reist oder den Mantis zu deinem täglichen Begleiter machst, mit der Instant Sharing Funktion kannst du deine Fotos und Videos direkt aus der App in die sozialen Medien posten und deine Freunde an deinem persönlichen Abenteuer teilhaben lassen. Lächel deiner Drohne einfach entgegen – dann macht sie ein Foto von dir sobald sie ein Gesicht erkennt.</p>\r\n<h3><br />Bis zu 4K Videoaufnahmen</h3>\r\n<p><br />Bilder macht der Mantis übrigens mit einer Auflösung von 4160×2340 (16:9) bzw. 4160x3120 (4:3) Pixeln im JPEG oder – wenn du sie später nachbearbeiten möchtest – auch im DNG-Format und speichert diese, genau wie die in bis zu 4K aufgezeichneten Videos, auf der mitgelieferten MicroSD-Karte. Videos werden dabei bis zu 1920x1080 („Full HD“) live und systemseitig über drei Achsen elektronisch stabilisiert. Das EIS-System ist energiesparend, solide und kommt ohne fragile Mechanik aus.</p>', 15, 157),
(202, 'Mavic Mini', '<h4>Kompakt und transportabel</h4>\r\n<h3>Fast so leicht wie ein Smartphone</h3>\r\n<p>Mit einem Gewicht von unter 250 g ist die Mavic Mini ein Leichtgewicht und nicht viel schwerer als ein gewöhnliches Smartphone. Dies macht die Mavic Mini nicht nur extrem portabel, sondern bringt sie auch in die niedrigste Gewichtsklasse der Drohnen, was in manchen Ländern weniger gesetzliche Regularien bedeutet.</p>\r\n<h4>Benutzerfreundlich</h4>\r\n<h3>Fliegen wie ein Profi.<br />In Windeseile.</h3>\r\n<p>Die neue DJI Fly App ist benutzerfreundlich und intuitiv. Mit DJI Fly erstellt jeder kinoreife Aufnahmen, leichter denn je zuvor! Die App bietet hilfreiche Tutorials, welche den Einstieg in die Flugwelt mit der Mavic Mini leicht, schnell und sicher machen.</p>\r\n<h4>Sicherer Flug</h4>\r\n<h3>Weniger Sorgen, mehr Zeit in der Luft!</h3>\r\n<p>Die Mavic Mini bietet 360° Propellerschützer, welche vollständigen Schutz vor den Propellern gewährleisten.[5] Dank fortschrittlicher Sensoren kann die Mavic Mini stabil schweben!</p>\r\n<h4>Einfache Bearbeitungsvorlagen</h4>\r\n<h3>Der App die Arbeit überlassen</h3>\r\n<p>Die DJI Fly App bietet eine große Auswahl an Vorlagen zur Videobearbeitung - grandios bearbeitete Videos auf Knopfdruck! Auch Einsteiger mit wenig Wissen in der Videobearbeitung, ihre Aufnahme in einen Social-Media-Hit.</p>\r\n<p> </p>', 9, 158);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `drone_camera`
--

CREATE TABLE `drone_camera` (
  `id` int(11) NOT NULL,
  `drone_id` int(11) NOT NULL,
  `camera_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `drone_camera`
--

INSERT INTO `drone_camera` (`id`, `drone_id`, `camera_id`) VALUES
(117, 199, 45),
(118, 199, 47),
(119, 199, 48);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `drone_category`
--

CREATE TABLE `drone_category` (
  `id` int(11) NOT NULL,
  `drone_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `drone_category`
--

INSERT INTO `drone_category` (`id`, `drone_id`, `category_id`) VALUES
(712, 185, 14),
(713, 185, 15),
(719, 186, 2),
(720, 186, 4),
(721, 186, 8),
(722, 186, 12),
(723, 186, 15),
(724, 187, 2),
(725, 187, 8),
(726, 187, 14),
(727, 188, 3),
(728, 188, 10),
(729, 188, 11),
(730, 189, 30),
(731, 189, 32),
(732, 189, 33),
(733, 189, 35),
(734, 189, 41),
(735, 189, 42),
(736, 190, 29),
(737, 190, 30),
(738, 190, 33),
(739, 190, 40),
(740, 190, 41),
(741, 190, 42),
(742, 191, 30),
(743, 191, 33),
(744, 191, 34),
(745, 191, 35),
(746, 192, 30),
(747, 192, 32),
(748, 192, 35),
(749, 192, 41),
(802, 193, 29),
(803, 193, 32),
(804, 193, 33),
(805, 193, 35),
(806, 193, 39),
(807, 193, 40),
(808, 193, 41),
(809, 193, 42),
(810, 194, 29),
(811, 194, 31),
(812, 194, 35),
(813, 194, 40),
(814, 194, 42),
(815, 195, 30),
(816, 195, 32),
(817, 195, 35),
(818, 195, 38),
(819, 195, 39),
(820, 195, 40),
(821, 195, 41),
(822, 195, 42),
(823, 196, 30),
(824, 196, 35),
(825, 196, 38),
(826, 196, 40),
(827, 196, 41),
(828, 196, 42),
(829, 197, 29),
(830, 197, 31),
(831, 197, 33),
(832, 197, 35),
(833, 197, 40),
(834, 197, 41),
(835, 197, 42),
(843, 198, 29),
(844, 198, 30),
(845, 198, 33),
(846, 198, 35),
(847, 198, 40),
(848, 198, 41),
(849, 198, 42),
(850, 199, 29),
(851, 199, 30),
(852, 199, 31),
(853, 199, 33),
(854, 199, 34),
(855, 199, 35),
(856, 199, 36),
(857, 199, 37),
(858, 199, 40),
(859, 199, 42),
(860, 200, 29),
(861, 200, 30),
(862, 200, 32),
(863, 200, 33),
(864, 200, 35),
(865, 200, 39),
(866, 200, 40),
(867, 200, 41),
(868, 200, 42),
(887, 201, 30),
(888, 201, 32),
(889, 201, 35),
(890, 201, 38),
(891, 201, 39),
(892, 201, 41),
(893, 202, 30),
(894, 202, 32),
(895, 202, 33),
(896, 202, 35),
(897, 202, 39),
(898, 202, 40),
(899, 202, 41),
(900, 202, 42);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `homepageimages`
--

CREATE TABLE `homepageimages` (
  `id` int(11) NOT NULL,
  `set_name` varchar(64) NOT NULL,
  `a_teaser_url` varchar(255) DEFAULT NULL,
  `a_alt_tag` varchar(255) NOT NULL,
  `a_title_tag` varchar(255) NOT NULL,
  `a_link` varchar(255) NOT NULL,
  `b_teaser_url` varchar(255) DEFAULT NULL,
  `b_alt_tag` varchar(255) NOT NULL,
  `b_title_tag` varchar(255) NOT NULL,
  `b_link` varchar(255) NOT NULL,
  `c_teaser_url` varchar(255) DEFAULT NULL,
  `c_alt_tag` varchar(255) NOT NULL,
  `c_title_tag` varchar(255) NOT NULL,
  `c_link` varchar(255) NOT NULL,
  `d_teaser_url` varchar(255) DEFAULT NULL,
  `d_alt_tag` varchar(255) NOT NULL,
  `d_title_tag` varchar(255) NOT NULL,
  `d_link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `homepageimages`
--

INSERT INTO `homepageimages` (`id`, `set_name`, `a_teaser_url`, `a_alt_tag`, `a_title_tag`, `a_link`, `b_teaser_url`, `b_alt_tag`, `b_title_tag`, `b_link`, `c_teaser_url`, `c_alt_tag`, `c_title_tag`, `c_link`, `d_teaser_url`, `d_alt_tag`, `d_title_tag`, `d_link`) VALUES
(18, 'Set Drones', 'public/assets/homepage/A-Teaser.png', 'DJI Inspire 2', 'DJI Inspire 2', 'drones/droneDetails/197', 'public/assets/homepage/B-Teaser.png', 'Kameras', 'Kameras entdecken', 'cameras', 'public/assets/homepage/C-Teaser.png', 'Drohnen', 'Drohnen entdecken', 'drones', 'public/assets/homepage/D-Teaser.png', 'DJI Xenmuse X5S', 'DJI Xenmuse X5S', 'cameras/cameraDetails/52'),
(19, 'Set Drone 2', 'public/assets/homepage/A-Teaser-2.png', 'DJI Mavic 2 Pro', 'DJI Mavic 2 Pro', 'drones/droneDetails/193', 'public/assets/homepage/B-Teaser-2.png', 'DJI Mavic Mini', 'DJI Mavic Mini', 'drones/droneDetails/202', 'public/assets/homepage/C-Teaser-2.png', 'Kameras', 'Kameras entdecken', 'cameras', 'public/assets/homepage/D-Teaser-2.png', 'Yuneec E10T Themalcamera', 'Yuneec E10T', 'cameras/cameraDetails/51');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `thumb_url` varchar(255) NOT NULL,
  `size` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `images`
--

INSERT INTO `images` (`id`, `name`, `image_url`, `thumb_url`, `size`, `created`, `updated`) VALUES
(137, 'canon mark 3 d5.png', 'uploads/images/2019/11/07/1573116883-7401.png', 'uploads/images/2019/11/07/1573116883-7401-thumb.png', 1178231, '2019-11-07 08:54:43', '2019-11-07 08:54:43'),
(138, 'yuneec_ion_l1_pro_camera.jpg', 'uploads/images/2019/11/07/1573117193-8758.jpg', 'uploads/images/2019/11/07/1573117193-8758-thumb.jpg', 104661, '2019-11-07 08:59:53', '2019-11-07 08:59:53'),
(139, 'nikon-d5.png', 'uploads/images/2019/11/07/1573117277-3731.png', 'uploads/images/2019/11/07/1573117277-3731-thumb.png', 315765, '2019-11-07 09:01:17', '2019-11-07 09:01:17'),
(140, 'xenmuse-xt2.png', 'uploads/images/2019/11/07/1573117435-8356.png', 'uploads/images/2019/11/07/1573117435-8356-thumb.png', 38058, '2019-11-07 09:03:55', '2019-11-07 09:03:55'),
(141, 'E90_inspection_camera-yuneec.png', 'uploads/images/2019/11/07/1573117562-8025.png', 'uploads/images/2019/11/07/1573117562-8025-thumb.png', 379982, '2019-11-07 09:06:02', '2019-11-07 09:06:02'),
(142, 'Yuneec-E50-Inspection-Camera.png', 'uploads/images/2019/11/07/1573117655-8601.png', 'uploads/images/2019/11/07/1573117655-8601-thumb.png', 92222, '2019-11-07 09:07:35', '2019-11-07 09:07:35'),
(143, 'mavic-pro.png', 'uploads/images/2019/11/07/1573117752-8476.png', 'uploads/images/2019/11/07/1573117752-8476-thumb.png', 138304, '2019-11-07 09:09:13', '2019-11-07 09:09:13'),
(144, 'phantom-4-obsidian.png', 'uploads/images/2019/11/07/1573117878-4585.png', 'uploads/images/2019/11/07/1573117878-4585-thumb.png', 128550, '2019-11-07 09:11:18', '2019-11-07 09:11:18'),
(145, 'gopro-karma.png', 'uploads/images/2019/11/07/1573117935-8103.png', 'uploads/images/2019/11/07/1573117935-8103-thumb.png', 184611, '2019-11-07 09:12:15', '2019-11-07 09:12:15'),
(146, 'spark.png', 'uploads/images/2019/11/07/1573118038-2214.png', 'uploads/images/2019/11/07/1573118038-2214-thumb.png', 54198, '2019-11-07 09:13:58', '2019-11-07 09:13:58'),
(149, 'mavic 2.png', 'uploads/images/2019/11/07/1573118494-8413.png', 'uploads/images/2019/11/07/1573118494-8413-thumb.png', 461252, '2019-11-07 09:21:34', '2019-11-07 09:21:34'),
(150, 'tornado-h920.png', 'uploads/images/2019/11/07/1573118821-5188.png', 'uploads/images/2019/11/07/1573118821-5188-thumb.png', 456153, '2019-11-07 09:27:01', '2019-11-07 09:27:01'),
(151, 'mavic-air.png', 'uploads/images/2019/11/07/1573118907-3439.png', 'uploads/images/2019/11/07/1573118907-3439-thumb.png', 106804, '2019-11-07 09:28:28', '2019-11-07 09:28:28'),
(152, 'mi-drone.png', 'uploads/images/2019/11/07/1573118995-9682.png', 'uploads/images/2019/11/07/1573118995-9682-thumb.png', 516225, '2019-11-07 09:29:55', '2019-11-07 09:29:55'),
(153, 'Inspire-1.png', 'uploads/images/2019/11/07/1573119092-3095.png', 'uploads/images/2019/11/07/1573119092-3095-thumb.png', 121272, '2019-11-07 09:31:32', '2019-11-07 09:31:32'),
(154, 'phantom-4-pro.png', 'uploads/images/2019/11/07/1573119156-5181.png', 'uploads/images/2019/11/07/1573119156-5181-thumb.png', 97417, '2019-11-07 09:32:36', '2019-11-07 09:32:36'),
(155, 'dji-matrice-drone-for-developer.png', 'uploads/images/2019/11/07/1573119351-3838.png', 'uploads/images/2019/11/07/1573119351-3838-thumb.png', 69508, '2019-11-07 09:35:51', '2019-11-07 09:35:51'),
(156, 'mavic-pro-platinum.png', 'uploads/images/2019/11/07/1573119446-8035.png', 'uploads/images/2019/11/07/1573119446-8035-thumb.png', 221273, '2019-11-07 09:37:26', '2019-11-07 09:37:26'),
(157, 'Mantis-q.png', 'uploads/images/2019/11/07/1573121242-5142.png', 'uploads/images/2019/11/07/1573121242-5142-thumb.png', 208466, '2019-11-07 10:07:22', '2019-11-07 10:07:22'),
(158, 'mavic-mini.png', 'uploads/images/2019/11/07/1573135931-1245.png', 'uploads/images/2019/11/07/1573135931-1245-thumb.png', 349802, '2019-11-07 14:12:11', '2019-11-07 14:12:11'),
(159, 'e10t_yuneec.png', 'uploads/images/2019/11/07/1573136200-8544.png', 'uploads/images/2019/11/07/1573136200-8544-thumb.png', 390573, '2019-11-07 14:16:40', '2019-11-07 14:16:40'),
(160, 'Xenmuse xs.png', 'uploads/images/2019/11/07/1573137744-1607.png', 'uploads/images/2019/11/07/1573137744-1607-thumb.png', 155139, '2019-11-07 14:42:24', '2019-11-07 14:42:24');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(64) NOT NULL,
  `lastname` varchar(64) NOT NULL,
  `email` varchar(120) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `email`, `password`, `role`, `created`, `updated`) VALUES
(8, 'Luca', 'Fiorentino', 'achilleas@gmail.com', '$2y$10$uWyLmJwR3mAvx69crRv6oeD2N.pJF8xi.jB1TUHcuOY0.U1jP8Md2', 1, '2019-09-21 10:02:16', '2019-10-13 14:41:25'),
(10, 'Niklas', 'Berend', 'NiklasBerend@gmail.de', '$2y$10$6VBWv.nXAU8XHZ6aDYTxl.5fxMUkVqZKV2Nl6sNy7D5io4WFLbASy', 1, '2019-10-14 16:53:18', '2019-11-07 08:00:58'),
(11, 'Giorgio', 'Mancusi', 'MancusiGiorgio@gmail.com', '$2y$10$DR2D9PBSye/ANQ89RcDz/e43tuL13jfe8Rg3dPro4m6DdzCAwW5mi', NULL, '2019-11-07 15:42:25', '2019-11-07 15:42:53');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `camera`
--
ALTER TABLE `camera`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `camera_category`
--
ALTER TABLE `camera_category`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `drone`
--
ALTER TABLE `drone`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `drone_camera`
--
ALTER TABLE `drone_camera`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `drone_category`
--
ALTER TABLE `drone_category`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `homepageimages`
--
ALTER TABLE `homepageimages`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT für Tabelle `camera`
--
ALTER TABLE `camera`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT für Tabelle `camera_category`
--
ALTER TABLE `camera_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT für Tabelle `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT für Tabelle `drone`
--
ALTER TABLE `drone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=204;

--
-- AUTO_INCREMENT für Tabelle `drone_camera`
--
ALTER TABLE `drone_camera`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT für Tabelle `drone_category`
--
ALTER TABLE `drone_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=904;

--
-- AUTO_INCREMENT für Tabelle `homepageimages`
--
ALTER TABLE `homepageimages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT für Tabelle `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;

--
-- AUTO_INCREMENT für Tabelle `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
