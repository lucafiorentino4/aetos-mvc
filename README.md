    ****************************************
    *                                      *
    *             Aetos                    *
    *        By: Luca Fiorentino           *
    *                                      *
    ****************************************

# About Aetos (English)
----------------------
Aetos is a Project I did during Summer Semester 2019 while I was studying Web Development in the SAE University in Hamburg, Germany.
The task was to set up a responsive website using PHP and MySQL with its own CMS, frontend and backend.
In addition, a database should be created, which should contain all relevant data.
The update of the data in the database should be possible via an extra layout, which is only accessible for logged in editors / admins.
Additionally a layout for unregistered users should be created.
A login form should be created with which the user can log in, as well as a registration form with which unregistered users can log out.
This should be developed so that the data arrives securely and encrypted to the database.
This should be based on OOP and the MVC methodology.
This task was given to students who have attended the subject Database and Backend Development.

## About the Project

This project is a website created using PHP & MySQL based on OOP and the MVC methodology.
The site is intended to appeal to anyone interested in cameras and
drones.
But also those who already own a camera or drone,
can use the large selection of objects on AETOS to inform themselves
compare, or e.g. check, if and which cameras are
and which cameras are still supported by their own drone, for which
for which purpose it was built or can be used at all
was built or can be used, such as for film productions,
for hobby use and much more.
The site can also provide good technical information to those who are new to this field.

The unregistered user* has the possibility to
- navigate on different subpages
- view individual drones/cameras
- look at the drones to see which cameras they support. He/she can reach these via click.
- create an account

The registered user*in without admin rights has the possibility:
- edit his/her profile

![](doc/aetos-mvc-user.gif)

The registered user with admin rights has the possibility to add drones, cameras, brands or categories:
- add
- delete
- edit
- link categories and brands to cameras and drones
- edit profile

![](doc/aetos-mvc-admin-user.gif)

## Configuration and initialization

    ******************************************************    
    *    Admin Login:                                    *             
    *                                                    *
    *    |                 |`USER DATA:`            |    *
    *    |-----------------|------------------------|    *
    *    |E-mail:          |`NiklasBerend@gmail.de` |    *
    *    |Password:        |`NiklasBerend2PW`       |    *
    *    |Role:            |`Admin: 1`              |    *
    *                                                    *
    ******************************************************

### STEP 1
--------------------------------------------------------------------------------------
In the "**Config**" file located in **path: *"Aetos-MVC\src\config "*** update the PORT (define('DB_PORT', **'my port'**);)

### STEP 2
--------------------------------------------------------------------------------------
Go to *"**http://localhost:8888/phpMyAdmin/?lang=de**"* and create a new "**User account**" under the menu *User accounts*.
and add there the data from the config file:

    Username: aetos_mvc_db
    Hostname: -> Lokal
    Passwort: aetos_mvc_pw

under "**User account database**"

       1 check mark

*this gives you all rights and creates a database with the same name as the username*   

and press **OK** in the lower right corner.    

### STEP 3
--------------------------------------------------------------------------------------
Click on the database "**aetos_mvc_db**" and import the **dump** I created.
this is located under: *"**Aetos-MVC/misc/sql/dump**"*
then go to phpMyAdmin and click on "**import**" there add the file and click on "**OK**".

### STEP 4
--------------------------------------------------------------------------------------
From the folder *"**Aetos-MVC/misc/**"* move the "**upload**" folder in the "**aetos-mvc**" folder which is in the "**htdocs**" folder.

--------------------------------------------------------------------------------------
### End Config


## Data splitting in the database from the back office area
### STEP 1
--------------------------------------------------------------------------------------
Open the file I created *"**Aetos-MVC/doc/Daten-objecte-pflege-in-db.xlsx**"*

**Description file**
There are 4 slides one for **brand**, one for **category**, one for **drone** and one for **cameras**.

**Images for maintenance**
You can find them under: *"**Aetos-MVC/doc/img**"* and then either "**drohnen-bilder**" or "**kameras-bilder**".

### STEP 2
--------------------------------------------------------------------------------------
Make a login with your account -> | **E-mail:** `NiklasBerend@gmail.de` | **Password: `NiklasBerend2PW`** |

1) first create a new **brand** ->
go to the **Back-Office** page "**/aetos-mvc/admin/showInfosDetail?mode=category**"
-> click on **New Item**
and add the data from the file *"**Aetos-MVC/doc/Daten-objecte-pflege-in-db.xlsx**"* from the slide "**category**"

2)  create a new **category** ->
  go to **back-office** page "**/aetos-mvc/admin/showInfosDetail?mode=brands**"
  -> click on **New Item**
  and add the data from the file *"**Aetos-MVC/doc/Daten-objecte-pflege-in-db.xlsx**"* from the slide "**brand**"

3) then create a new **camera** ->
  go to the **back-office** page "**/aetos-mvc/adminCameras/showCamerasDetail?mode=cameras**"
  -> click on **New camera**
  and add the data from the file *"**Aetos-MVC/doc/Daten-objecte-pflege-in-db.xlsx**"* from the slide "**camera**"

4) then create a new **drone** ->
  go to the **back-office** page "**/aetos-mvc/adminDrones/openNewDroneForm?mode=drones**"
  -> click on **New drone**
  and add the data from the file *"**Aetos-MVC/doc/Daten-objecte-pflege-in-db.xlsx**"* from the slide "**drone**"

## Copyrights
This project was upload strictly to show my work and what I've learned during my studies. This, copying it or use it in any way is not allowed.

----------------------

# Über Aetos (Deutsch)
----------------------
Aetos ist ein Projekt, das ich im Sommersemester 2019 während meines Studiums der Webentwicklung an der SAE University in Hamburg durchgeführt habe.
Die Aufgabe war das aufsetzen einer responsiven Webseite mittels PHP und MySQL mit eigenem CMS, Frontend und Backend.
Dazu sollte eine Datenbank erstellt werden, die alle Relevanten Daten beinhalten sollte.
Die Aktualisierung der Daten in der Datenbank sollen über einen Extra Layout möglich sein, die nur für eingelogte Editoren/ Admins erreichbar ist.
Zusätzlich soll einen Layout für unregistrierte Nutzer erstellt werden.
Es soll ein Login Formular erstellt werden mit sich der Nutzer einloggen kann, sowie auch ein Registrierungsformular, über sich nicht registrierte Nutzer sich abmelden können.
Dies soll so Entwickelt werden das die Daten sicher und Verschlüsselt zur Datenbank ankommen.
Dies soll auf basis von OOP und der MVC-Methodik erstellt werden.
Diese Aufgabe wurde an Studenten vergeben, die das Fach Datenbank- und Backend-Entwicklung besucht haben.
## Über das Projekt

Dieses Projekt ist eine Webseite, die mittels PHP & MySQL erstellt wurde auf Basis von OOP und der MVC-Methodik.
Die Seite soll alle Personen, die sich für Kameras und
Drohnen interessieren, ansprechen.
Aber auch wer eine Kamera oder Drohne schon besitzt,
kann sich auf AETOS durch die große Auswahl an Objekten
informieren, vergleichen, oder z.B. überprüfen, ob
und welche Kameras von der eigenen Drohne noch unterstützt
wird, für welchen Zweck sie überhaupt gebaut
wurde oder benutzt werden kann, wie z.B. für Filmproduktionen,
für die Hobbynutzung und vieles mehr.
Die Seite kann auch die Personen, die neu in diesem Bereich sind, gut mit technischen Informationen versorgen.

Der unregistrierte Nutzer*in hat die Möglichkeit:
- auf verschiedenen Unterseiten zu navigieren
- sich einzelne Drohnen/ Kameras angucken
- sich bei den Drohnen anzusehen, welche Kameras diese unterstützt. Diese kann er/ sie per click erreichen.
- ein Konto anzulegen

Der registrierte Nutzer*in ohne Admin Rechten hat die Möglichkeit:
- sein Profil zu bearbeiten

![](doc/aetos-mvc-user.gif)

Der registrierte Nutzer*in mit Admin Rechten hat die Möglichkeit Drohnen, Kameras, Marken oder Kategorien:
- hinzuzufügen
- zu löschen
- zu bearbeiten
- Kategorien und Marken an Kameras und Drohnen verknüpfen
- Profil zu bearbeiten

![](doc/aetos-mvc-admin-user.gif)

## Configuration und Initialisierung


    ******************************************************    
    *    Admin Zugangsdaten:                             *             
    *                                                    *
    *    |                 |`USER DATA:`            |    *
    *    |-----------------|------------------------|    *
    *    |E-mail:          |`NiklasBerend@gmail.de` |    *
    *    |Password:        |`NiklasBerend2PW`       |    *
    *    |Role:            |`Admin: 1`              |    *
    *                                                    *
    ******************************************************

### STEP 1
--------------------------------------------------------------------------------------
Aktualisiere in der "**Config**" Datei die im **pfad: *"Aetos-MVC\src\config"*** liegt den PORT (define('DB_PORT', **'mein Port'**);)

### STEP 2
--------------------------------------------------------------------------------------
Gehe auf *"**http://localhost:8888/phpMyAdmin/?lang=de**"* und erstelle ein neues "**Benutzerkonto**" unter den Menü *Benutzerkonten*
und füge dort die Daten aus der Config Datei hinzu:

    Benutzername: aetos_mvc_db
    Hostname: -> Lokal
    Passwort: aetos_mvc_pw

unter "**Datenbank für Benutzerkonto**"

       1 Häcken setzen

*das gibt dir alle Rechte und erstellt eine Datenbank mit dem gleichen Namen wie den Benutzernamen*   

und auf **OK** unten rechts drücken.      

### STEP 3
--------------------------------------------------------------------------------------
Clicke auf die Datenbank "**aetos_mvc_db**" und importiere den vom mir erstellten **dump**
dieser liegt unter: *"**Aetos-MVC/misc/sql/dump**"*
dann gehe auf phpMyAdmin und clicke auf "**importieren**" dort die Datei hinzufügen und auf "**OK**" klicken.

### STEP 4
--------------------------------------------------------------------------------------
Vom Ordner *"**Aetos-MVC/misc/**"* das "**upload**" Ordner im "**aetos-mvc**" Ordner der im "**htdocs**" Ordner ist, verschieben.

--------------------------------------------------------------------------------------
##### Ende Config

## Datenplege in der Datenbank aus dem Backofficebereich
### STEP 1
--------------------------------------------------------------------------------------
Öffne die von mir erstellte Datei *"**Aetos-MVC/doc/Daten-objecte-pflege-in-db.xlsx**"*

**Beschreibung Datei**
Es gibt 4 Folien eine für **brand**, eine für **category**, eine für **drone** und eine für **cameras**.

**Bilder für die Pflege**
Die findest du unter: *"**Aetos-MVC/doc/img**"* und dann entweder "**drohnen-bilder**" oder "**kameras-bilder**"

### STEP 2
--------------------------------------------------------------------------------------
Mach ein Login mit Dein Account -> | **E-mail:** `NiklasBerend@gmail.de` | **Password: `NiklasBerend2PW`** |

1) erstelle als erstes ein neues **brand** ->
gehe auf die **Back-Office** Seite "**/aetos-mvc/admin/showInfosDetail?mode=category**"
-> clicke auf **New Item**
und füge die Daten aus der Datei *"**Aetos-MVC/doc/Daten-objecte-pflege-in-db.xlsx**"* aus der Folie "**category**"

2)  erstelle ein neue **category** ->
  gehe auf die **Back-Office** Seite "**/aetos-mvc/admin/showInfosDetail?mode=brands**"
  -> clicke auf **New Item**
  und füge die Daten aus der Datei *"**Aetos-MVC/doc/Daten-objecte-pflege-in-db.xlsx**"* aus der Folie "**brand**"

3) dann erstelle eine neue **camera** ->
  gehe auf die **Back-Office** Seite "**/aetos-mvc/adminCameras/showCamerasDetail?mode=cameras**"
  -> clicke auf **New camera**
  und füge die Daten aus der Datei *"**Aetos-MVC/doc/Daten-objecte-pflege-in-db.xlsx**"*aus der Folie "**camera**"

4) dann erstelle eine neue **drone** ->
  gehe auf die **Back-Office** Seite "**/aetos-mvc/adminDrones/openNewDroneForm?mode=drones**"
  -> clicke auf **New drone**
  und füge die Daten aus der Datei *"**Aetos-MVC/doc/Daten-objecte-pflege-in-db.xlsx**"* aus der Folie "**drone**"

##  Copyrights
Dieses Projekt wurde ausschließlich hochgeladen, um meine Arbeit und das, was ich während meines Studiums gelernt habe, zu zeigen. Daher ist es nicht erlaubt, es zu kopieren oder in irgendeiner Weise zu verwenden.
