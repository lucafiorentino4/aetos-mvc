<?php


class Database extends PDO {

    public function __construct() {

        $dsn = DB_TYPE . ':host=' . DB_HOST . ';port=' . DB_PORT . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET;

        $user = DB_USER;
        $pass = DB_PASS;

        parent::__construct($dsn, $user, $pass /*, options */);

    }

}