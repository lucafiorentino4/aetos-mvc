<?php

abstract class PrivateController extends Controller {

    public function __construct() {

        $this->view = new PrivateView();

        Session::set('controller', get_class($this));

    }

}