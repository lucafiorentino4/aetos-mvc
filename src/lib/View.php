<?php


class View {

    protected $rendered = false;

    function render($element){

        if (!$this->rendered){

            // include header
            require "partial/header.php";
            // include navigation
            require "partial/navbar.php";
            // include messages
            require "partial/messages.php";
            // include page content
            require "view/$element.php";
            // include debug
            //require "partial/debug.php";
            // include footer
            require "partial/footer.php";

        // hier sage ich wenn die bedingungen erfüllt wurden in diesen fall das rendern der elemente
        // dann änder $rendered auf true, das wir immer bei jeden reload neu auf false gesetzt
            $this->rendered = true;
        }


    }

}