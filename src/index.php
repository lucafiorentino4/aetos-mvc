<?php

include "config/config.php";


include "lib/Helper.php";
include "lib/Debug.php";

include "lib/Session.php";
Session::start();

include "lib/Message.php";

include "lib/File.php";

include "lib/Database.php";

include "lib/Model.php";

include "lib/View.php";
include "lib/PrivateView.php";

include "lib/Controller.php";
include "lib/PrivateController.php";


include "lib/Application.php";

$app = new Application();
