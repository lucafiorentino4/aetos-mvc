// scripts.js


// Script Navi D->M
$('.ui.sidebar').sidebar({
    context: $('.ui.pushable'),
    transition: 'overlay'
}).sidebar('attach events', '#mobile_item');


// SEMANTIC_UI CONFIG --------------------------------------------------------------------------------------------------
$('.tabular.menu .item').tab();

$('.menu.item').tab();

$('.dropdown').dropdown();

$('.ui.dropdown').dropdown();

$('#select').dropdown();


// TINYMCE CONFIG --------------------------------------------------------------------------------------------------
tinymce.init({
    selector: '.tinymce-editor',
    entity_encoding: "raw",
});




