<?php

    $messages = Message::getAll();

    $deleteIndex = -1;

    if (isset($_GET['deleteMsg'])) {

        $deleteIndex = $_GET['deleteMsg'];

        Message::remove($deleteIndex);

    }


?>

<div class="ui message-margin container">

    <?php foreach ($messages as $index => $message): ?>

        <div class="ui <?=$message['size']?> icon message <?=$message['class']?>">
            <?php if ($message['icon']): ?>
                <i class="<?=$message['icon']?> icon"></i>
            <?php endif; ?>
            <a class="close icon" href="<?=Helper::addUrlParam('deleteMsg', $index)?>" style="color: inherit;">
                <i class="close icon <?= ($deleteIndex == $index) ? 'remove' : '' ?>"></i>
            </a>
            <div class="content">
                <div class="header">
                    <?=$message['header']?>
                </div>
                <p>
                    <?=$message['content']?>
                </p>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<script>
   $('.message .close.icon.remove').closest('.message').transition('fade');
</script>
