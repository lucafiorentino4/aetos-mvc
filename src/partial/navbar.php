<?php
    $controller = Session::get('controller');
    $user = Session::get('user');

    // styling of the navigation
    $adminActive = ($controller == 'Admin' || $controller == 'AdminCameras' || $controller == 'AdminDrones') ? 'active red' : ' ';
    $adminUrl = URL . 'admin';
    $adminButton = "<a class=\"item  $adminActive \" href=\" $adminUrl \"><b>Back-Office</b></a>";

    //search bar settings
    $search = isset($_GET['search']) ? trim($_GET['search']) : '';
    $searchLink = ($controller == 'Drones') ? URL . "drones/search" :  URL . "cameras/search";
?>

  <div class="ui grid">
    <div class="computer only row">
        <div class="column">
            <div class="ui inverted menu heightMenu">
                <a class="item" id="logo-D" href="<?=URL?>">
                    <img src="<?= URL . LOGO ?>" alt="">
                </a>
                <a class="item <?= ($controller == 'Drones') ? ' CIYellow' : ' ' ?>" href="<?=URL?>drones">
                    <b>Drohnen</b>
                </a>
                <a class="item <?= ($controller == 'Cameras') ? ' CIYellow' : ' ' ?>" href="<?=URL?>cameras">
                    <b>Kameras</b>
                </a>
                <?= ($user['role'] !== null ) ? $adminButton : ' ' ?>
                <div class="right menu">
                    <?php if($controller == 'Drones' ||$controller == 'Cameras'): ?>
                        <div class="item">
                            <form action="<?=$searchLink?>" method="get">
                                <div class="ui icon input">
                                    <input type="text"  name="search" placeholder="Search..."  value="<?=$search?>">
                                    <i class="search link icon"></i>
                                </div>
                            </form>
                        </div>
                    <?php endif; ?>
                    <?php if ($user): ?>
                        <a class="ui item" href="<?=URL?>userProfile/"><b>Profil von <?=$user['firstname']?></b></a>
                        <a class="ui item" href="<?=URL?>auth/doLogout">
                           <i class="user yellow icon"></i>Logout
                        </a>
                    <?php else: ?>
                        <div class="item">
                            <a href="<?=URL?>auth/">
                              <i class="user white icon"></i>Login
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

        </div>
    </div>

    <div class="tablet mobile only row">
        <div class="column">
            <div class="ui inverted menu">
                <a id="mobile_item" class="item">
                    <i class="bars large icon"></i>
                </a>
                <a class="logo-M" href="<?=URL?>">
                    <img src="<?= URL . LOGO ?>" alt="">
                </a>
                <?php if ($user): ?>
                <!-- logged in -->
                    <a id="mobile_item" class="user-M item right" href="<?=URL?>userProfile/">
                        <i class="user large yellow icon"></i>
                    </a>
                <?php else: ?>
                <!-- logged out -->
                    <a id="mobile_item" class="user-M item right" href="<?=URL?>auth/">
                        <i class="user large white icon"></i>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<div class="ui pushable">
    <div class="ui sidebar inverted vertical menu">
        <a class="item <?= ($controller == 'Drones') ? 'active blue' : ' ' ?>" href="<?=URL?>drones">
            <b>Drones</b>
        </a>
        <a class="item <?= ($controller == 'Cameras') ? 'active blue' : ' ' ?>" href="<?=URL?>cameras">
            <b>Cameras</b>
        </a>
        <?= ($user['role'] !== null ) ? $adminButton : ' ' ?>
        <a class="item" href="<?=URL?>auth/doLogout">Logout</a>

    </div>
    <div class="pusher">
        <div id="content" >
            <!-- content start -->

