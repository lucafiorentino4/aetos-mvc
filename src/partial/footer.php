                             <!-- container -->
                        </div>
                    </div>
                </div>
            <script src="<?=URL?>public/js/script.js"></script>
        </div>
         <div class="ui inverted vertical footer segment marginTop">
             <div class="ui container">
                 <div class="ui stackable inverted divided equal height stackable grid">
                     <div class="three wide column">
                         <h4 class="ui inverted header">Fakos</h4>
                         <div class="ui inverted link list">
                             <a href="<?=URL?>drones" class="item">Drohnen</a>
                             <a href="<?=URL?>cameras" class="item">Cameras</a>
                         </div>
                     </div>
                     <div class="three wide column">
                         <h4 class="ui inverted header">Specials</h4>
                         <div class="ui inverted link list">
                             <a href="<?=URL?>drones/droneDetails/197" class="item">DJI Inspire</a>
                             <a href="<?=URL?>drones/droneDetails/193" class="item">DJI Mavic 2</a>
                             <a href="<?=URL?>cameras/cameraDetails/47" class="item">Nikon D5</a>
                         </div>
                     </div>
                     <div class="seven wide column">
                         <h4 class="ui inverted header">Impressum</h4>
                         <p>© 2019 Lorem Ipsum Deutschland. Alle Rechte vorbehalten. Lorem Ipsum, 22393 Hamburg</p>
                     </div>
                 </div>
             </div>
         </div>
       </div>
   </body>
</html>