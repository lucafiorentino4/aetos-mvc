<?php


error_reporting(E_ALL);
ini_set('display_errors', 1);

//----------------------------------------------------------------------------------------------------------------------
// MySQL Settings

define('DB_TYPE', 'mysql');
define('DB_HOST', 'localhost');
define('DB_PORT', '8889');


define('DB_NAME', 'aetos_mvc_db');
define('DB_USER', 'aetos_mvc_db');
define('DB_PASS', 'aetos_mvc_pw');

define('DB_CHARSET', 'utf8');

//----------------------------------------------------------------------------------------------------------------------

define('URL', '/aetos-mvc/');

//----------------------------------------------------------------------------------------------------------------------

define('IMAGE_UPLOADS_PATH', 'uploads/images');
define('IMAGE_THUMB_NAME', '-thumb');
define('IMAGE_THUMB_EXT', '.jpg');

define('IMAGE_THUMB_WIDTH', 200);
define('IMAGE_THUMB_HEIGHT', 200);

define('USER_IMAGE_PLACEHOLDER', 'public/assets/image/userPlaceholder.png');
define('IMAGE_PLACEHOLDER', 'public/assets/image/placeholder.png');
define('IMAGE_PLACEHOLDER_THUMB', 'public/assets/image/placeholder-thumb.png');

define('LOGO', 'public/assets/logo/Logo.svg');
define('FAVICON', 'public/assets/logo/favicon.png');


