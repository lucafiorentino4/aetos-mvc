<?php


class Home_Model extends Model {

    // get from db one teaser set randomly
    public function getRandomTeaserSet(){

        $sql = "SELECT *
        FROM homepageimages AS hi
        ORDER BY RAND()
        LIMIT 1;";

        $obj = $this->db->prepare($sql);

        $obj->execute();

        $result = $obj->fetch(PDO::FETCH_ASSOC);

        return $result;
    }
}