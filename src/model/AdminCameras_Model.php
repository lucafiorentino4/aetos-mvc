<?php


class AdminCameras_Model extends Model{

    // validation new camera data
    function validateAddNewCameraData($formdata){
        $error = array();

        foreach ($formdata as $key => $value) {
            if (empty($value)) $error[$key] = 'This field can not be empty';
        }

        return !empty($error) ? $error : false;
    }

    // Get all Properties for Cameras from Category
    public function getPropsForCameras($type, $camera_id){
        switch ($type){
            case 'category':
                $sql = "SELECT camcat.category_id AS id, cat.name
                         FROM camera_category AS camcat
                         LEFT JOIN category AS cat ON cat.id = camcat.category_id
                         WHERE camera_id = :id;";
                break;
        }
        $obj = $this->db->prepare($sql);

        $obj->execute(array(
            ':id' => $camera_id
        ));
        $result = $obj->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    // Get Data from camera
    public function getCamerasFromTable(){
        $sql = "SELECT c.id, i.thumb_url AS image, c.name, b.name AS brand, c.text
                FROM camera AS c 
                LEFT JOIN brand AS b ON c.brand_id = b.id
                LEFT JOIN images AS i ON i.id = c.image_id;";

        $obj = $this->db->prepare($sql);

        $obj->execute();
        $cameras = $obj->fetchAll(PDO::FETCH_ASSOC);

        // add category to all drones
        foreach ($cameras as $index => $camera){
            $cameras[$index]['category'] = $this->getPropsForCameras('category', $camera['id']);
        }

        return $cameras;
    }

    // get all camera, brand or category data from db
    public function getAllDataFromTable($tableName){
        switch ($tableName) {
            case 'cameras':
                $sql = "SELECT * FROM camera;";
                break;
            case 'brand':
                $sql = "SELECT * FROM brand;";
                break;
            case 'category':
                $sql = "SELECT * FROM category;";
                break;
            default:
                return false;
        }
        $obj = $this->db->prepare($sql);
        $obj->execute();
        $result = $obj->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    // functions for edit camera table
    // add pictures
    public function addImageToDB($upload_file){
        $sql = "INSERT INTO images (name, image_url, thumb_url, size)
                 VALUES (:name, :image_url, :thumb_url, :size);";

        $obj = $this->db->prepare($sql);

        $result = $obj->execute(array(
            ':name' => $upload_file['name'],
            ':image_url' => $upload_file['image'],
            ':thumb_url' => $upload_file['thumb'],
            ':size' => $upload_file['size']
        ));

        return ($result) ? $this->db->lastInsertId() : false;
    }

    // connect props to camera
    public function addCameraProps($camera_id, $array_data){
        $sql = "INSERT INTO camera_category (camera_id, category_id) VALUES (:camera_id, :value_id);";

        foreach ($array_data as $value_id){
            $obj = $this->db->prepare($sql);

            $obj->execute(array(
                ':camera_id' => $camera_id,
                ':value_id' => $value_id
            ));
        }
    }

    // für update, damit die Daten beim updaten angezeigt werden
    public function getAllDataFromCamera($camera_id){

        $sql = "SELECT c.* , b.name AS brand, i.image_url
                 FROM camera AS c
                 LEFT JOIN brand AS b ON b.id = c.brand_id
                 LEFT JOIN images AS i ON i.id = c.image_id
                 WHERE c.id = :camera_id;";

        $obj = $this->db->prepare($sql);

        $obj->execute(array(
            ':camera_id' => $camera_id
        ));

        $camera = $obj->fetch(PDO::FETCH_ASSOC);
        $camera['category'] = $this->getPropsForCameras('category', $camera_id);

        return $camera;
    }

    // this function get an image from database
    public function getImageFromTable($id){

       $sql = 'SELECT c.image_id, i.image_url, i.thumb_url
                FROM  camera AS c
                LEFT JOIN images AS i ON i.id = c.image_id
                WHERE c.id = :camera_id;';

        $obj = $this->db->prepare($sql);
        $obj->execute(array(
            ':camera_id' => $id
        ));

        $cameraData = $obj->fetch(PDO::FETCH_ASSOC);

        return $cameraData;
    }

    // add new camera in db
    public function addNewCamera($name, $formdata, $upload_file){

        $image_id = ($upload_file) ? $this->addImageToDB($upload_file) : 0;


        $sql = 'INSERT INTO camera (name, brand_id, text, image_id) VALUES (:name, :brand_id, :text, :image_id);';


        $obj = $this->db->prepare($sql);

        $result = $obj->execute(array(
            ':name' => $name,
            ':brand_id' => $formdata['brand'],
            ':text' => $formdata['text'],
            ':image_id' => $image_id
        ));

        $camera_id = $this->db->lastInsertId();
        $this->addCameraProps($camera_id, $formdata['category']);

        return $result;
    }

    // Delete cameras & drones + Camera Props
    public function deleteCameraProps($type, $camera_id){
        switch ($type){
            case 'category':
                $sql = "DELETE FROM camera_category 
                    WHERE camera_id  = :camera_id;";
                break;
        }
        $obj = $this->db->prepare($sql);

        $result = $obj->execute(array(
            ':camera_id' => $camera_id
        ));

        // to screen if the function return true or false
        return $result;
    }

    // function that delete the selected camera from db
    public function adminDeleteCamera($camera_id){

        // Delete all categorys or other Props
        $this->deleteCameraProps('category', $camera_id);
        // get the old set image from db
        $oldImage = $this->getImageFromTable($camera_id);
        // if image exist use the function delete file, for delete the image and the thumb from the upload folder
        if($oldImage['image_id']){
            $this->deleteFileAndImage($oldImage['image_id'], $oldImage['image_url'], $oldImage['thumb_url']);
        }

        // Delete the camera
        $sql = 'DELETE FROM camera WHERE id = :camera_id;';

        $obj = $this->db->prepare($sql);
        $result = $obj->execute(array(
            ':camera_id' => $camera_id
        ));

        return $result;
    }

    // delete file image from upload Folder and from db
    public function deleteFileAndImage($id, $image, $thumb){
        //Delete all Files
        File::delete($image);
        File::delete($thumb);

        //Delete image from DB
        $obj = $this->db->prepare('DELETE FROM images WHERE id = :id');
        $result = $obj->execute(array(':id' => $id));

        return $result;
    }

    // update cameras & drones
    public function updatetingCamera($id, $name, $formdata, $upload_file){

        // Delete all categorys or other Props
        $this->deleteCameraProps('category', $id);

        $oldImage = $this->getImageFromTable($id);

        if($upload_file){
            if($oldImage['image_id'])
                $this->deleteFileAndImage($oldImage['image_id'], $oldImage['image_url'], $oldImage['thumb_url']);
            // Add New Picture
            $image_id = $this->addImageToDB($upload_file);
        } else{
            //hold old image
            $image_id = $oldImage['image_id'];
        }


        $sql = 'UPDATE camera SET name = :name, brand_id = :brand_id, text = :text, image_id = :image_id WHERE camera.id = :id;';

        $obj = $this->db->prepare($sql);


        $result = $obj->execute(array(
            ':name' => $name,
            ':brand_id' => $formdata['brand'],
            ':text' => $formdata['text'],
            ':image_id' => $image_id,
            ':id' => $id
        ));

        $this->addCameraProps($id, $formdata['category']);

        return $result;
    }
}