<?php


class UserProfile_Model extends Model{

    // Get user data by id
    public function getUserData($id){

        $sql = "SELECT * FROM user WHERE user.id = :id";
        $obj = $this->db->prepare($sql);
        $obj->execute(array(
            ':id' => $id
        ));

        $result = $obj->fetch(PDO::FETCH_ASSOC);

        //delete password
        unset($result['password']);


        return $result;
    }

    // update user data in db
    public function aktualizeUser($formdata, $userID){

        $sql = "UPDATE user SET firstname = :firstname, lastname = :lastname, email = :email WHERE user.id = :id";

        $obj = $this->db->prepare($sql);
        $result = $obj->execute(array(
            ':firstname' => $formdata['firstname'],
            ':lastname' => $formdata['lastname'],
            ':email' => $formdata['email'],
            ':id' => $userID
        ));

        return $result;
    }

    // get the user by email
    function getUserByEmail($email){
        $sql='SELECT * FROM user WHERE email = :email LIMIT 1;';

        $obj = $this->db->prepare($sql);

        $obj->execute(array(
            ':email' => $email
        ));

        $result = $obj->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    // check if the email already exist
    function userEmailExist($email){
        return (!!$this->getUserByEmail($email));
    }

    // check if the password is right
    function checkPassword($formdata, $userEmail){

        $user = $this->getUserByEmail($userEmail);

        if ($userEmail) {
            $result = password_verify($formdata['password'],  $user['password']);
            if (!$result) {
                //debug::add($result, 'result');
                return false;
            }
            unset($user['password']);

            return $user;
        } else {
            return false;
        }
    }

    // register form validation
    function validateFormData($formdata, $userEmail){

        $error = array();

        foreach ($formdata as $key => $value){
           if(empty($value)) $error[$key] = 'This field is empty';
        }

        if(!($formdata['email'] == $userEmail)){
            if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
                $error[$key] = 'This E-Mail is not valid';
            }
            else {
                if ($this->userEmailExist($formdata['email'])) $error[$key] = 'This E-Mail is already Taken';
            }
        }

       // usage of debug
       // debug::add($formdata, 'formdata');
       // debug::add($error, 'errors');
       // debug::add($value, 'value');


        return !empty($error) ? $error : false;
    }
}