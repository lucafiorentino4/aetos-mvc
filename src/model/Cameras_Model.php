<?php


class Cameras_Model extends Model{
    // Get properties for a camera
    public function getPropsForCameras($camera_id){

        $sql = "SELECT camcat.category_id AS id, cat.name
                 FROM camera_category AS camcat
                 LEFT JOIN category AS cat ON cat.id = camcat.category_id
                 WHERE camera_id = :id;";

        $obj = $this->db->prepare($sql);

        $obj->execute(array(
            ':id' => $camera_id
        ));
        $result = $obj->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    // Get all cameras
    public function getCamerasFromTable($search = ''){

        $sql = "SELECT c.id, i.image_url AS image,  c.name, b.name AS brand, c.text
                FROM camera AS c 
                LEFT JOIN brand AS b ON c.brand_id = b.id            
                LEFT JOIN images AS i ON i.id = c.image_id
                WHERE c.name LIKE :search OR c.text LIKE :search OR b.name LIKE :search;";

        $obj = $this->db->prepare($sql);

        $obj->execute(array(
            ':search' => "%$search%"
        ));
        $cameras = $obj->fetchAll(PDO::FETCH_ASSOC);

        // add category to all cameras
        foreach ($cameras as $index => $camera){
            $cameras[$index]['category'] = $this->getPropsForCameras($camera['id']);
        }

        return $cameras;
    }

    // get all data from one camera by id
    public function getAllDataFromCamera($camera_id){

        $sql = "SELECT c.* , b.name AS brand, i.image_url, c.text
                 FROM camera AS c
                 LEFT JOIN brand AS b ON b.id = c.brand_id
                 LEFT JOIN images AS i ON i.id = c.image_id
                 WHERE c.id = :camera_id;";

        $obj = $this->db->prepare($sql);

        $obj->execute(array(
            ':camera_id' => $camera_id
        ));

        $camera = $obj->fetch(PDO::FETCH_ASSOC);
        $camera['category'] = $this->getPropsForCameras( $camera_id);

        return $camera;
    }

}
