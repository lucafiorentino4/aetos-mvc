<?php


class Auth_Model extends Model{

    // get user from email
    function getUserFromEmail($email){
        $sql='
        SELECT * FROM user WHERE email = :email LIMIT 1;
        ';

        $obj = $this->db->prepare($sql);
        $obj->execute(array(
            ':email' => $email
        ));

        $result = $obj->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    // check if the email already exist
    function userEmailExist($email){
        return (!!$this->getUserFromEmail($email));
    }

    // validate insert data for login
    function loginUser($formdata){

        $user = $this->getUserFromEmail($formdata['email']);

        if ($user) {

            $result = password_verify($formdata['password'],  $user['password']);

            if (!$result) {
                return false;
            }

            unset($user['password']);

            return $user;

        } else {
            return false;
        }
    }

    // register form validation
    function validateFormData($formdata){

        $error = array();

        foreach ($formdata as $key => $value){
            if(empty($value)) $error[$key] = 'This field is empty';

            switch ($key){
                case 'email':
                    //check if the email is a valid email
                    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
                    $error[$key] = 'This E-Mail is not valid';}
                    else {
                        //check if the email already exist in the db
                        if ($this->userEmailExist($value)) $error[$key] = 'This E-Mail is already Taken';
                    }
                    break;
                case 'password':
                    //check if the email is longer than 8 characters
                    if (strlen($formdata[$key]) < 8)
                        $error[$key] = 'This password is not valid';
                    break;
                case 'password2':
                    if($formdata['password'] != $formdata['password2'])
                        $error[$key] = 'The two passwords do not match';
                    break;
            }
        }
        return !empty($error) ? $error : false;
    }

    // insert the validate data in the database
    function insertUserInDB($user) {

        $sql = 'INSERT INTO user (firstname, lastname, email, password) VALUES (:firstname, :lastname, :email, :password);';

        $obj = $this->db->prepare($sql);

        $hashedPassword = password_hash($user['password'], PASSWORD_DEFAULT);

        $result = $obj->execute(array(
            ':firstname' => $user['firstname'],
            ':lastname' => $user['lastname'],
            ':email' => $user['email'],
            ':password' => $hashedPassword
        ));

        return $result;
    }
}