<?php


class AdminDrones_Model extends Model{

    // validate new insert drone
    function validateAddNewDroneData($formdata){
        $error = array();

        foreach ($formdata as $key => $value) {
            if (empty($value)) $error[$key] = 'This field can not be empty';
        }

        return !empty($error) ? $error : false;
    }

    // Get properties category or cameras for drone
    public function getPropsForDrones($type, $drone_id){
        switch ($type){
            case 'category':
                $sql = "SELECT dcat.category_id AS id, cat.name
                         FROM drone_category AS dcat
                         LEFT JOIN category AS cat ON cat.id = dcat.category_id
                         WHERE drone_id = :id;";
                break;
            case 'cameras':
                $sql = "SELECT dcam.camera_id AS id, c.name
                         FROM drone_camera AS dcam
                         LEFT JOIN camera AS c ON c.id = dcam.camera_id
                         WHERE drone_id = :id;";
                break;
        }
        $obj = $this->db->prepare($sql);

        $obj->execute(array(
            ':id' => $drone_id
        ));
        $result = $obj->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    // Get all Data from drones
    public function getDronesFromTable(){

        $sql = "SELECT d.id, i.thumb_url AS image,  d.name, b.name AS brand, d.text
                FROM drone AS d 
                LEFT JOIN brand AS b ON d.brand_id = b.id            
                LEFT JOIN images AS i ON i.id = d.image_id;";

        $obj = $this->db->prepare($sql);

        $obj->execute();
        $objects = $obj->fetchAll(PDO::FETCH_ASSOC);

        // add category to all drones
        foreach ($objects as $index => $object){
            $objects[$index]['cameras'] = $this->getPropsForDrones('cameras', $object['id']);
            $objects[$index]['category'] = $this->getPropsForDrones('category', $object['id']);
        }

        return $objects;
    }

    // get all data from one of the selected tables
    public function getAllDataFromTable($tableName){
        switch ($tableName) {
            case 'cameras':
                $sql = "SELECT * FROM camera;";
                break;
            case 'brand':
                $sql = "SELECT * FROM brand;";
                break;
            case 'category':
                $sql = "SELECT * FROM category;";
                break;
            default:
                return false;
        }
        $obj = $this->db->prepare($sql);
        $obj->execute();
        $result = $obj->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    // add pictures
    public function addImageToDB($upload_file){
        $sql = "INSERT INTO images (name, image_url, thumb_url, size)
                 VALUES (:name, :image_url, :thumb_url, :size);";

        $obj = $this->db->prepare($sql);

        $result = $obj->execute(array(
            ':name' => $upload_file['name'],
            ':image_url' => $upload_file['image'],
            ':thumb_url' => $upload_file['thumb'],
            ':size' => $upload_file['size']
        ));

        return ($result) ? $this->db->lastInsertId() : false;
    }

    // add props to the table drone_category or drone_camera
    public function addDroneProps($type, $drone_id, $array_data){
        switch ($type){
            case "category":
                $sql = "INSERT INTO drone_category (drone_id, category_id) VALUES (:drone_id, :value_id);";
                break;
            case "cameras":
                $sql = "INSERT INTO drone_camera (drone_id, camera_id) VALUES (:drone_id, :value_id);";
                break;
        }

        foreach ($array_data as $value_id){
            $obj = $this->db->prepare($sql);

            $obj->execute(array(
                ':drone_id' => $drone_id,
                ':value_id' => $value_id
            ));
        }
    }

    // get all data from drones for show it in the update view
    public function getAllDataFromDrones($drones_id){

        $sql = "SELECT d.* , b.name AS brand, i.image_url 
                 FROM drone AS d
                 LEFT JOIN brand AS b ON b.id = d.brand_id
                 LEFT JOIN images AS i ON i.id = d.image_id
                 WHERE d.id = :drones_id;";

        $obj = $this->db->prepare($sql);

        $obj->execute(array(
            ':drones_id' => $drones_id
        ));

        $object = $obj->fetch(PDO::FETCH_ASSOC);
        $object['category'] = $this->getPropsForDrones('category', $drones_id);
        $object['cameras'] = $this->getPropsForDrones('cameras', $drones_id);

        return $object;
    }

    // get the image by id from db
    public function getImageFromTable($id){

        $sql = 'SELECT d.image_id, i.image_url, i.thumb_url
                FROM  drone AS d
                LEFT JOIN images AS i ON i.id = d.image_id
                WHERE d.id = :drone_id;';

        $obj = $this->db->prepare($sql);
        $obj->execute(array(
            ':drone_id' => $id
        ));

        $DroneData = $obj->fetch(PDO::FETCH_ASSOC);

        return $DroneData;
    }

    // add new drone in db
    public function addNewDrone($name, $formdata, $upload_file){

        $image_id = ($upload_file) ? $this->addImageToDB($upload_file) : 0;
        $categoryValue = ($formdata['category']) ?  $formdata['category'] : null;
        $camerasValue = ($formdata['cameras']) ?  $formdata['cameras'] : null;

        $sql = 'INSERT INTO drone (name, brand_id, text, image_id) VALUES (:name, :brand_id, :text, :image_id);';


        $obj = $this->db->prepare($sql);

        $result = $obj->execute(array(
            ':name' => $name,
            ':brand_id' => $formdata['brand'],
            ':text' => $formdata['text'],
            ':image_id' => $image_id,
        ));

        $drone_id = $this->db->lastInsertId();

        $this->addDroneProps('category', $drone_id, $categoryValue);
        $this->addDroneProps('cameras', $drone_id, $camerasValue);

        return $result;
    }

    // Delete cameras & drones from props table drone_category or drone_camera
    public function deleteDroneProps($type, $drone_id){
        switch ($type){
            case 'category':
                $sql = "DELETE FROM drone_category 
                    WHERE drone_id  = :drone_id;";
                break;
            case 'cameras':
                $sql = "DELETE FROM drone_camera 
                    WHERE drone_id  = :drone_id;";
                break;
        }

        $obj = $this->db->prepare($sql);

        $result = $obj->execute(array(
            ':drone_id' => $drone_id
        ));

        return $result;
    }

    // Delete drone from db by id
    public function adminDeleteDrone($drone_id){

        // Delete all prop from the tables drone_category or drone_camera
        $this->deleteDroneProps('category', $drone_id);
        $this->deleteDroneProps('cameras', $drone_id);

        // get the old insert image
        $oldImage = $this->getImageFromTable($drone_id);

        // if there is a id from a images than delete it from the db and from the upload folder
        if($oldImage['image_id']){
            $this->deleteFileAndImage($oldImage['image_id'], $oldImage['image_url'], $oldImage['thumb_url']);
        }

        // Delete the drone from db
        $sql = 'DELETE FROM drone WHERE id = :drone_id;';

        $obj = $this->db->prepare($sql);
        $result = $obj->execute(array(
            ':drone_id' => $drone_id
        ));

        return $result;
    }

    // delete file from upload folder, and image from db
    public function deleteFileAndImage($id, $image, $thumb){
        //Delete all Files
        File::delete($image);
        File::delete($thumb);

        //Delete image from DB
        $obj = $this->db->prepare('DELETE FROM images WHERE id = :id');
        $result = $obj->execute(array(':id' => $id));

        return $result;
    }

    // update cameras & drones
    public function updatetingDrone($id, $name, $formdata, $upload_file){

        // Delete all prop from the tables drone_category or drone_camera
        $this->deleteDroneProps('category', $id);
        $this->deleteDroneProps('cameras', $id);

        // get the old insert image
        $oldImage = $this->getImageFromTable($id);

        // if there is a new insert picture, than delete the old picture and set the new one
        if($upload_file){
            // if exist an old picture delete it from db and from upload folder
            if($oldImage['image_id'])
                $this->deleteFileAndImage($oldImage['image_id'], $oldImage['image_url'], $oldImage['thumb_url']);
            // Add New Picture
            $image_id = $this->addImageToDB($upload_file);
        } else{
            //hold old image
            $image_id = $oldImage['image_id'];
        }


        $sql = 'UPDATE drone SET name = :name, brand_id = :brand_id, text = :text, image_id = :image_id WHERE drone.id = :id;';

        $obj = $this->db->prepare($sql);

        $result = $obj->execute(array(
            ':name' => $name,
            ':brand_id' => $formdata['brand'],
            ':text' => $formdata['text'],
            ':image_id' => $image_id,
            ':id' => $id
        ));

        // add new props
        $this->addDroneProps('category', $id, $formdata['category']);
        $this->addDroneProps('cameras', $id, $formdata['cameras']);

        return $result;
    }
}