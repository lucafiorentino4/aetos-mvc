<?php


class Admin_Model extends Model{

    // get all data from the tables brand or category
    public function getInfosFromTable($table_name){
        switch ($table_name) {
            case 'brands':
                $sql = 'SELECT * FROM brand;';
                break;
            case 'category':
                $sql = 'SELECT * FROM category;';
                break;
            default:
                return false;
        }
        $obj = $this->db->prepare($sql);
        $obj->execute();
        $result = $obj->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //get all from the table brand or category
    public function getAllDataFromTable($tableName){
        switch ($tableName) {
            case 'brand':
                $sql = "SELECT * FROM brand;";
                break;
            case 'category':
                $sql = "SELECT * FROM category;";
                break;
            default:
                return false;
        }
        $obj = $this->db->prepare($sql);
        $obj->execute();
        $result = $obj->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    // get Row from info tables, like category and brands
    public function getRow($element, $id)
    {
        switch ($element) {
            case 'brands':
                $insertTable = 'brand';
                break;
            case 'category':
                $insertTable = 'category';
                break;
            default:
                return false;
        }
        $sql = "SELECT tname.*
                FROM $insertTable AS tname
                WHERE tname.id = :id;";

        $obj = $this->db->prepare($sql);

        $obj->execute(array(
            ':id' => $id
        ));
        $result = $obj->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    // delete akk data from the selected brand or category
    public function adminDeleteInfos($id, $element){
        switch ($element) {
            case 'brands':
                $sql = 'DELETE FROM brand WHERE id = :id;';
                break;
            case 'category':
                $sql = 'DELETE FROM category WHERE id = :id;';
                break;
            default:
                return false;
        }
        $obj = $this->db->prepare($sql);
        $result = $obj->execute(array(
            ':id' => $id
        ));

        return $result;
    }

    // update the infos of brand or category in the db
    public function updatetingInfos($id, $element, $name){
        switch ($element) {
            case 'brands':
                $sql = 'UPDATE brand SET name = :name WHERE brand.id = :id;';
                break;
            case 'category':
                $sql = 'UPDATE category SET name = :name WHERE category.id = :id;';
                break;
            default:
                return false;
        }

        $obj = $this->db->prepare($sql);

        $result = $obj->execute(array(
            ':name' => $name,
            ':id' => $id
        ));

        return $result;
    }

    // add new element in brands or category
    public function addNewInfos($element, $name){
        switch ($element) {
            case 'brands':
                $sql = 'INSERT INTO brand (name) VALUES (:name);';
                break;
            case 'category':
                $sql = 'INSERT INTO category (name) VALUES (:name);';
                break;
            default:
                return false;
        }

        $obj = $this->db->prepare($sql);
        $result = $obj->execute(array(
            ':name' => $name
        ));
        return $result;
    }

}

