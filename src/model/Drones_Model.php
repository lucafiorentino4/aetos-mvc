<?php


class Drones_Model extends Model{
    // Get all Properties for drone from the table drone_category or drone_camera
    public function getPropsForDrones($type, $drone_id){
        switch ($type){
            case 'category':
                $sql = "SELECT dcat.category_id AS id, cat.name
                         FROM drone_category AS dcat
                         LEFT JOIN category AS cat ON cat.id = dcat.category_id
                         WHERE drone_id = :id;";
                break;
            case 'cameras':
                $sql = "SELECT dcam.camera_id AS id, c.name, i.image_url, i.thumb_url, brand_id
                         FROM drone_camera AS dcam
                         LEFT JOIN camera AS c ON c.id = dcam.camera_id
                         LEFT JOIN images AS i ON c.image_id = i.id
                         WHERE drone_id = :id;";
                break;
        }
        $obj = $this->db->prepare($sql);

        $obj->execute(array(
            ':id' => $drone_id
        ));
        $result = $obj->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    // Get all data from drone
    public function getDronesFromTable($search = ''){

        $sql = "SELECT d.id, i.image_url AS image,  d.name, b.name AS brand, d.text
                FROM drone AS d 
                LEFT JOIN brand AS b ON d.brand_id = b.id            
                LEFT JOIN images AS i ON i.id = d.image_id
                WHERE d.name LIKE :search OR d.text LIKE :search OR b.name LIKE :search;";

        $obj = $this->db->prepare($sql);

        $obj->execute(array(
            ':search' => "%$search%"
        ));

        $objects = $obj->fetchAll(PDO::FETCH_ASSOC);

        // get categorys and cameras to all drones
        foreach ($objects as $index => $object){
            $objects[$index]['cameras'] = $this->getPropsForDrones('cameras', $object['id']);
            $objects[$index]['category'] = $this->getPropsForDrones('category', $object['id']);
        }

        return $objects;
    }

    // Get all data from one drone by id
    public function getAllDataFromDrones($drones_id){

        $sql = "SELECT d.* , b.name AS brand, i.image_url
                 FROM drone AS d
                 LEFT JOIN brand AS b ON b.id = d.brand_id
                 LEFT JOIN images AS i ON i.id = d.image_id
                 WHERE d.id = :drones_id;";

        $obj = $this->db->prepare($sql);

        $obj->execute(array(
            ':drones_id' => $drones_id
        ));

        $object = $obj->fetch(PDO::FETCH_ASSOC);
        $object['category'] = $this->getPropsForDrones('category', $drones_id);
        $object['cameras'] = $this->getPropsForDrones('cameras', $drones_id);

        return $object;
    }
}