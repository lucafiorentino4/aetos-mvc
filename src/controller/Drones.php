<?php


class Drones extends Controller{
    // Drones controller -> here I ask the Data from the Drones_Model for the Drones View

    // Search Function that makes possible the User to search stuff on the page
    public function search() {

        $search = trim($_GET['search']);

        $this->view->search = $search;
        $this->view->allDrones = $this->model->getDronesFromTable($search);


        $this->view->render('drones/index');

    }

    // Aks Data for the Selected Drone and send it to the Drone Detail Page view
    public function droneDetails($drone_id) {

        $allDroneData = $this->model->getAllDataFromDrones($drone_id);

        $this->view->allDroneData = $allDroneData;

        $this->view->render('drones/droneDetailsPage');

    }

    // Render the Drones view and send all the Drones that are in the Database to the view
    public function index(){

        $allDrones = $this->model->getDronesFromTable();
        $this->view->allDrones = $allDrones;

        $this->view->render('drones/index');
    }

}