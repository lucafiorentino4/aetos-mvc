<?php

class AdminDrones extends PrivateController {

    // get all infos for create a new drone, and render the view
    public function openNewDroneForm(){
        $mode = $_GET['mode'];

        $brand = $this->model->getAllDataFromTable('brand');
        $category = $this->model->getAllDataFromTable('category');
        $cameras = $this->model->getAllDataFromTable('cameras');

        $this->view->brand = $brand;
        $this->view->category = $category;
        $this->view->cameras = $cameras;

        $this->view->render('adminDrones/aktion/addNewDrone');
    }

    // send the formdata from the new drone to the model for create a new element in the db
    public function addNewDroneToDB(){
        $mode = $_GET['mode'];
        $name = trim($_POST['name']);
        $formdata = $_POST;
        $file = $_FILES['image'];

        $validateFormData = [
            'name' => $formdata['name'],
            'brand' => $formdata['brand']];

        $error = $this->model->validateAddNewDroneData($validateFormData);

        if(!$error){

            if($file['name'] != ' '){
                $upload_file = File::uploadImg($file);
            } else {
                $upload_file = null;
            }

            $result = $this->model->addNewDrone($name, $formdata, $upload_file);

            if ($result) {
                Message::add(ucfirst($mode) . ' added', '<code>' . $name . ' </code> created as new ' . ucfirst($mode), 'blue', 'cart plus');
                header('Location: ' . URL . "adminDrones/showDronesDetail?mode=$mode");
                return;
            }
        }

        $this->view->error = $error;
        $this->view->insertData = $_POST;

        $this->openNewDroneForm();
    }

    // send the id to the model for delete the drone
    public function deleteDrone($id){
        $mode = $_GET['mode'];

        $this->model->adminDeleteDrone($id, $mode);

        Message::add(ucfirst($mode) . ' deleted', 'ID: <code>' . $id . '</code> deleted', 'red', 'trash');
        header("Location: " . URL . "adminDrones/showDronesDetail?mode=$mode");
    }

    // ask the data from the model, and give the data to the new rendered view editDrone
    public function editDrone($id){
        $brand = $this->model->getAllDataFromTable('brand');
        $category = $this->model->getAllDataFromTable('category');
        $cameras = $this->model->getAllDataFromTable('cameras');
        $allDrones = $this->model->getAllDataFromDrones($id);

        $this->view->brand = $brand;
        $this->view->category = $category;
        $this->view->cameras = $cameras;
        $this->view->allDrones = $allDrones;

        $this->view->render('adminDrones/aktion/editDrone');
    }

    // send the updated data to the model for update drone
    public function updateDrone($id){
        $mode = $_GET['mode'];
        $name = trim($_POST['name']);
        $formdata = $_POST;

        if(!$name){
            header('Location: ' . URL . "adminDrones/editDrone/$id?mode=$mode");
            return;
        }

        $file = $_FILES['image'];

        if($file['name'] == ''){
            $this->model->updatetingDrone($id, $name, $formdata, null);
        } else {
            $upload_file = File::uploadImg($file);
            $this->model->updatetingDrone($id, $name, $formdata, $upload_file);
        }

        Message::add(ucfirst($mode) . ' update', '<code>' . $name . '</code> was updated', 'green', 'wrench');
        header('Location: ' . URL . "adminDrones/showDronesDetail?mode=$mode");
    }

    // get the data from db and prepare and render it
    public function showDronesDetail(){
        $mode = $_GET['mode'];

        if(!$mode){
            header('Location: ' . URL . 'admin/index');
            return;
        }

        $allDrones = $this->model->getDronesFromTable();
        $this->view->allDrones = $allDrones;

        $this->view->render('adminDrones/dronesList');
    }

    // render a table list of drones
    public function index(){
        $this->showDronesDetail();
    }
}