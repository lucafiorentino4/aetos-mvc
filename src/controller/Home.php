<?php


class Home extends Controller {

    // Homepage -> here i ask the Model the Teaser Sets
    // and render the Homepage
    public function index(){

        $teaserSet = $this->model->getRandomTeaserSet();

        $this->view->teaserSet = $teaserSet;

        $this->view->render('home/index');
    }
}