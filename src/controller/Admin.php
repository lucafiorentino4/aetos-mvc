<?php


class Admin extends PrivateController{
    // Admin controller -> here I ask the Data from the Admin_Model for the Private Back-Office View
    // only a User that have Role: 1;  can see this view, and change stuff

    // Open the view brand's or category's, check if the "mode" is brand's or category's and show their data
    public function showInfosDetail(){
        $mode = $_GET['mode'];
        if(!$mode) {
            header('Location: ' . URL . 'admin/index');
        }

        $info = $this->model->getInfosFromTable($mode);
        $this->view->info = $info;
        $this->view->render('admin/aktion/infosList');
    }

    // Open the view, with the form for add a new brand or category, check if the "mode" is brand's or category's and show their data
    public function openNewInfoForm(){
        $mode = $_GET['mode'];
        $this->view->render('admin/aktion/addNewInfo');
    }

    // This function send the formdata to the Admin Model
    public function addNewInfosToDB(){
        $mode = $_GET['mode'];
        $name = trim($_POST['name']);


        if(!$name) {
            header('Location: ' . URL . "admin/openNewInfoForm?mode=$mode");
            return;
        }

        $this->model->addNewInfos($mode, $name);

        Message::add(ucfirst($mode) . ' added', '<code>' . $name . ' </code> created as new ' . ucfirst($mode), 'blue', 'cart plus');

        header('Location: ' . URL . "admin/showInfosDetail?mode=$mode");
    }

    // This function render the view for edit a brand or category
    public function editInfos($id){
        $mode = $_GET['mode'];

        $row = $this->model->getRow($mode, $id);
        $brand = $this->model->getAllDataFromTable('brand');

        $this->view->row = $row;
        $this->view->brand = $brand;

        $this->view->render('admin/aktion/editInfos');
    }

    // This function send the updated data to the Admin_Model for update a brand or a model
    public function updateInfos($id){
        $mode = $_GET['mode'];
        $name = trim($_POST['name']);

        if(!$name){
            header('Location: ' . URL . "admin/editInfos/$id?mode=$mode");
            return;
        }

        $this->model->updatetingInfos($id, $mode, $name);
        Message::add(ucfirst($mode) . 'update', '<code>' . $name . '</code> was updated', 'green', 'wrench');
        header('Location: ' . URL . "admin/showInfosDetail?mode=$mode");
    }

    // This function send the id to the Admin_Model for delete a brand or a category
    public function deleteInfo($id){
        $mode = $_GET['mode'];
        $this->model->adminDeleteInfos($id, $mode);
        Message::add(ucfirst($mode) . ' deleted', 'ID: <code>' . $id . '</code> deleted', 'red', 'trash');
        header("Location: " . URL . "admin/showInfosDetail?mode=$mode");
    }

    // render admin page
    public function index(){
        $this->view->render('admin/index');
    }
}
