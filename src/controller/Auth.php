<?php


class Auth extends Controller{

    // This function doLogout and delete the user data from the session
    public function doLogout(){
        Session::remove('user');
        header('Location: ' . URL . 'auth/login');
    }

    // This function render the Login view
    public function login(){
        $this->view->render('auth/login');
    }

    // This function send the data to the Model for the validation and
    public function doLogin(){

        $user = $this->model->loginUser($_POST);

        if(!$user){

           $this->view->error = 'The insert Password or E-Mail is wrong, insert again';

           $this->view->render('auth/login');

           return;
        }

        Session::set('user', $user);

        $lastUrl = Session::get('lastUrl');
        $lastUrl = ($lastUrl) ? URL . $lastUrl : URL . 'home';
        Session::remove('lastUrl');


        $firstname = $user['firstname'];
        $lastname = $user['lastname'];

        Message::add('Login', "Welcome back deer $lastname $firstname", 'green', 'key');
        header('Location: ' . URL . 'home');

        return;
    }

    // render the register view
    public function register(){
        $this->view->render('auth/register');
    }

    // this function send the data to model for validation
    public function doRegister(){

        $error = $this->model->validateFormData($_POST);

        // if the validation return no errors than send the data to the Model
        if(!$error){

            $result = $this->model->insertUserInDB($_POST);

            // if the validation goes wrong
            // Helper::debug($_POST);
            if ($result) {
                header("Location: " . URL . "auth/login");
                return;
            }
        }

        $this->view->error = $error;
        $this->view->user = $_POST;
        $this->view->render('auth/register');
    }

// render the login view
public function index(){
    $this->login();
}
}