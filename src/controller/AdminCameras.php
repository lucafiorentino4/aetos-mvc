<?php

class AdminCameras extends PrivateController{

    // get all infos for create a new camera, and render the view
    public function openNewCameraForm(){
        $mode = $_GET['mode'];

        $brand = $this->model->getAllDataFromTable('brand');
        $category = $this->model->getAllDataFromTable('category');
        $cameras = $this->model->getAllDataFromTable('cameras');

        $this->view->brand = $brand;
        $this->view->category = $category;
        $this->view->cameras = $cameras;

        $this->view->render('adminCameras/aktion/addNewCamera');
    }

    // send the formdata from the new camera to the model for create a new element in the db
    public function addNewCameraToDB(){
        $mode = $_GET['mode'];
        $name = trim($_POST['name']);
        $formdata = $_POST;
        $file = $_FILES['image'];

        $validateFormData = [
            'name' => $formdata['name'],
            'brand' => $formdata['brand']];

        $error = $this->model->validateAddNewCameraData($validateFormData);


        if(!$error) {

            if ($file['name'] != ' ') {
                $upload_file = File::uploadImg($file);
            } else {
                $upload_file = null;
            }

            $result = $this->model->addNewCamera($name, $formdata, $upload_file);

            if ($result) {
                Message::add(ucfirst($mode) . ' added', '<code>' . $name . ' </code> created as new ' . ucfirst($mode), 'blue', 'cart plus');
                header('Location: ' . URL . "adminCameras/showCamerasDetail?mode=$mode");
                return;
            }
        }

        $this->view->error = $error;
        $this->view->insertData = $_POST;
        $this->openNewCameraForm();
    }

    // send the id to the model for delete the camera
    public function deleteCamera($id){
        $mode = $_GET['mode'];

        $this->model->adminDeleteCamera($id);

        Message::add(ucfirst($mode) . ' deleted', 'ID: <code>' . $id . '</code> deleted', 'red', 'trash');
        header("Location: " . URL . "adminCameras/showCamerasDetail?mode=$mode");
    }

    // ask the data from the model, and give the data to the new rendered view editCamera
    public function editCamera($id){

        $brand = $this->model->getAllDataFromTable('brand');
        $category = $this->model->getAllDataFromTable('category');
        $cameras = $this->model->getAllDataFromTable('cameras');
        $allCameras = $this->model->getAllDataFromCamera($id);

        $this->view->brand = $brand;
        $this->view->category = $category;
        $this->view->cameras = $cameras;
        $this->view->allCameras = $allCameras;

        $this->view->render('adminCameras/aktion/editCamera');
    }

    // send the updated data to the model for update camera
    public function updateCamera($id){
        $mode = $_GET['mode'];
        //get name from $_POST and trim it
        $name = trim($_POST['name']);
        $formdata = $_POST;

        if (!$name) {
            header('Location: ' . URL . "adminCameras/editCamera/$id?mode=$mode");
            return;
        }

        $file = $_FILES['image'];

        if ($file['name'] == '') {
            $this->model->updatetingCamera($id, $name, $formdata, null);
        }
        else {
            $upload_file = File::uploadImg($file);
            $this->model->updatetingCamera($id, $name, $formdata, $upload_file);
        }


        Message::add(ucfirst($mode) . ' update ', '<code>' . $name . '</code> was updated', 'green', 'wrench');
        header('Location: ' . URL . "adminCameras/showCamerasDetail?mode=$mode");
    }

    // get the data from db and prepare and render it
    public function showCamerasDetail(){
        $mode = $_GET['mode'];

        if (!$mode) {
            header('Location: ' . URL . 'admin/index');
            return;
        }

        $allCameras = $this->model->getCamerasFromTable();
        $this->view->allCameras = $allCameras;

        $this->view->render('adminCameras/camerasList');
    }

    // render a table list of cameras
    public function index(){
        $this->showCamerasDetail();
    }


}