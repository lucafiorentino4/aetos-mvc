<?php


class Cameras extends Controller {

    //cameras controller -> here I ask the Data from the Cameras_Model for the Cameras View

    //  Search Function that makes possible the User to search stuff on the page
    public function search() {
        $search = trim($_GET['search']);

        $this->view->search = $search;
        $this->view->allCameras = $this->model->getCamerasFromTable($search);


        $this->view->render('cameras/index');

    }

    // Aks Data for the Selected Camera and send it to the Camera Detail Page view
    public function cameraDetails($camera_id) {

        $allCameraData = $this->model->getAllDataFromCamera($camera_id);

        $this->view->allCameraData = $allCameraData;

        $this->view->render('cameras/cameraDetailsPage');
    }

    // Render the Camera view and send all the cameras that are in the Database to the view
    public function index(){
        $allCameras = $this->model->getCamerasFromTable();
        $this->view->allCameras = $allCameras;

        $this->view->render('cameras/index');
    }
}