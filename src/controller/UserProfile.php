<?php


class UserProfile extends PrivateController{
    // This Render the User Profile view, and send to the logged in User the Data of his Profile

    //this Function render the view to change the Data of his own Profile
    public function changeUserData(){
        $this->view->render('userProfile/action/editUser');
    }

    // this function get the insert data and send it to the Model
    public function aktualizeData(){

        // here I get the user ID of the logged in user and his E-Mail
        $userID = Session::get('user')['id'];

        $userEmail = Session::get('user')['email'];

        // here I send the insert Data to the Model for Validation and save possible errors
        $error = $this->model->validateFormData($_POST, $userEmail);

        // if there is no error than validate the insert Password with this of the user in the Database
        if (!$error) {
            // if the password is correct send the user back, if not than returns false
            $user = $this->model->checkPassword($_POST, $userEmail);

            //if the password is wrong than return the errors and do not actualize the profile
            // if the password is correct, than send the data to the database
            if(!$user){
                $error['password'] = 'Something goes wrong, try again.';
                $this->view->error = $error;
                $this->view->render('userProfile/action/editUser');
                return;
            } else {
                $result = $this->model->aktualizeUser($_POST, $userID);

                if ($result) {
                    $user = $this->model->getUserData($userID);
                    // reset the new data of the user
                    Session::set('user', $user);
                    $userName = Session::get('user')['firstname'];
                    Message::add(' Profile actualize', '<code>' .  ' </code> ' . ucfirst($userName) . ' you actualize your profile with success', 'green', 'user');
                    header("Location: " . URL . "userProfile/index");
                    return;
                }
                return;
            }
        }

        $this->view->error = $error;
        $this->view->user = $_POST;
        $this->view->render('userProfile/action/editUser');
    }

    // render the user Profile page
    public function index(){
        $this->view->render('userProfile/index');
    }

}