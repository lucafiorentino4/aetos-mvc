<?php
$error = isset($this->error) ? $this->error : null;

?>



<div class="ui centered grid">
    <div class="centered row">
        <h1 class="header">Login</h1>
    </div>

    <div class="centered row">
        <form class="ui four wide computer sixteen wide mobile eight wide tablet form column aligned centered" method="post" action="<?=URL?>auth/doLogin">
            <?= ($error) ? "<div class=\"ui red message\"><i class=\"exclamation triangle icon\"></i> $error </div>" : ' ' ?>
            <div class="field">
                <label>E-Mail</label>
                <input type="text" name="email" placeholder="Insert your E-Mail here">
            </div>
            <div class="field">
                <label>Password</label>
                <input type="password" name="password">
            </div>
            <button class="ui button CIYellow" type="submit">Login</button>
        </form>
    </div>

    <div class="centered  row">
        <div class="four wide computer sixteen wide mobile eight wide tablet column aligned centered">
            <a href="<?=URL?>auth/register">
                <button class="ui black basic button">
                   Falls Du neu bei uns bist, <u>bitte hier registrieren!</u>
                </button>
            </a>
        </div>
    </div>
</div>

