<?php

$user['firstname'] = isset($this->user['firstname']) ? $this->user['firstname'] : '';
$user['lastname'] = isset($this->user['lastname']) ? $this->user['lastname'] : '';
$user['email'] = isset($this->user['email']) ? $this->user['email'] : '';
$user['password'] = isset($this->user['password']) ? $this->user['password'] : '';

$error = isset($this->error) ? $this->error : null;

$errorLabel = array();

if ($error){
    foreach ($error as $key => $value){
       $ucFirstKey = ucfirst($key);
       $errorLabel[$key] = "<div class=\"ui pointing red basic label\"><i class=\"exclamation triangle icon\"></i> $ucFirstKey: $value</div>";
    }
}

?>

<div class="ui three column centered grid">
    <div class="two column centered row">
        <h1 class="header">Neu registrieren</h1>
    </div>

    <div class="centered row">
        <form class="ui eight wide computer sixteen wide mobile eight wide tablet form column" method="post" action="<?=URL?>auth/doRegister">
            <div class="field">
                <label>Vorname</label>
                <input type="text" name="firstname" value="<?= $user['firstname'] ?>">
                <?= isset($errorLabel['firstname']) ? $errorLabel['firstname'] : ' ' ?>
            </div>
            <div class="field">
                <label>Name</label>
                <input type="text" name="lastname" value="<?= $user['lastname'] ?>">
                <?= isset($errorLabel['lastname']) ? $errorLabel['lastname'] : ' ' ?>
            </div>
            <div class="field">
                <label>E-Mail</label>
                <input type="text" name="email" value="<?= $user['email'] ?>">
                <?= isset($errorLabel['email']) ? $errorLabel['email'] : '' ?>
            </div>
            <div class="field">
                <label>Passwort</label>
                <input type="password" name="password" value="<?= $user['password'] ?>">
                <?= isset($errorLabel['password']) ? $errorLabel['password'] : '' ?>
            </div>
            <div class="field">
                <label>Passwort wiederholen</label>
                <input type="password" name="password2">
                <?= isset($errorLabel['password2']) ? $errorLabel['password2'] : '' ?>
            </div>
            <button class="ui button CIYellow" type="submit">Registrieren</button>
        </form>
    </div>

    <div class="two column centered row">
        <div class="four column centered row">
            <a href="<?=URL?>auth/login">
                <button class="ui black basic button">
                    Falls Du schon angemeldet bist, <u>gehe zum login!</u>
                </button>
            </a>
        </div>
    </div>


</div>