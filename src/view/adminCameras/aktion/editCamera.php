<?php
$mode = $_GET['mode'];

//$element = $this->row;
$brand = $this->brand;
$category = $this->category;
$allCameras = $this->allCameras;

function checkForIdInArray($array, $object_id)
{
    foreach ($array as $row) {
        if ($row['id'] == $object_id)
            return $result = 'selected';
        }

        return $result = ' ';
}

$actionURL = URL . "adminCameras/updateCamera/" . $allCameras['id'] . "?mode=" . $mode;


?>

<div class="ui buttons ">
    <a class="ui labeled icon button black" href="<?=URL?>adminCameras/showCamerasDetail?mode=<?=$mode?>">
        <i class="caret left icon"></i>
        Zurück
    </a>
</div>

<h1>Update von "<?= ucfirst($allCameras['name']) ?>"</h1>

<form class="ui form" action="<?= $actionURL ?>" method="POST" enctype="multipart/form-data">
        <div class="ui grid field">
            <input type="hidden" name="MAX_FILE_SIZE" value="6000000">
            <div class="ui six desktop field wide">
                    <?php if($allCameras['image_url']): ?>
                        <img class="objectDetailThumb" src="<?=URL . $allCameras['image_url']?>" alt="">
                    <?php else: ?>
                        <img class="objectDetailThumb" src="<?=URL . IMAGE_PLACEHOLDER?>" alt="">
                    <?php endif; ?>
                <div class="field wide">
                    <label>Image</label>
                    <input type="file" name="image">
                </div>
            </div>
            <div class="ui ten field  wide">
                <div class="ui grid field wide">
                    <div class="ui eight desktop field wide">
                        <label>Name</label>
                        <input type="text" name="name"  value="<?=$allCameras['name']?>">
                    </div>
                    <div class="ui eight desktop field wide">
                        <label>Brand</label>
                        <select class="ui fluid dropdown" name="brand">
                            <option value="<?=$allCameras['brand_id']?>">Brand..</option>
                            <?php foreach($brand AS $row): ?>
                                <option value="<?=$row['id']?>" ><?= $row['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="ui sixteen desktop field wide">
                    <label>Category</label>
                    <select class="ui dropdown" name="category[]" multiple="">
                        <option value=""></option>
                        <?php foreach ($category as $row): ?>
                            <option value="<?=$row['id']?>" <?= checkForIdInArray($allCameras['category'], $row['id']); ?>><?= $row['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>


        </div>

    <div class="field">
        <label>Text</label>
        <textarea class="tinymce-editor" name="text"><?=$allCameras['text']?></textarea>
    </div>
    <button class="ui button CIYellow" type="submit">Submit</button>
</form>