<?php
$mode = $_GET['mode'];

$brand = $this->brand;
$category = $this->category;
$cameras = $this->cameras;

$insertData['brand'] = isset($this->insertData['brand']) ? $this->insertData['brand'] : '';
$insertData['name'] = isset($this->insertData['name']) ? $this->insertData['name'] : '';
$insertData['brand'] = isset($this->insertData['brand']) ? $this->insertData['brand'] : '';
$insertData['category'] = isset($this->insertData['category']) ? $this->insertData['category'] : '';
$insertData['cameras'] = isset($this->insertData['cameras']) ? $this->insertData['cameras'] : '';

function checkForIdInArray($array, $object_id){
    foreach ($array as $row) {
        if ($row == $object_id)
            return $result = 'selected';
    }
    return $result = '';
}

$actionURL = URL . "adminCameras/addNewCameraToDB/" . "?mode=" . $mode;

$error = isset($this->error) ? $this->error : null;

$errorLabel = array();

if ($error){
    //$error[$key] is like = for example error -> name,
    // the value is the error, for example 'this field is empty
    foreach ($error as $key => $value){
        $ucFirstKey = ucfirst($key);
        $errorLabel[$key] = "<div class=\"ui pointing red basic label\"><i class=\"exclamation triangle icon\"></i> $ucFirstKey: $value</div>";
    }
}
?>

<div class="ui buttons ">
    <a class="ui labeled icon button black" href="<?=URL?>adminCameras/showCamerasDetail?mode=<?=$mode?>">
        <i class="caret left icon"></i>
        Zurück
    </a>
</div>

<h2>Neue Kamera:</h2>

<form class="ui form" method="POST" action="<?= $actionURL ?>" enctype="multipart/form-data">

        <div class="two fields">
            <input type="hidden" name="MAX_FILE_SIZE" value="6000000">
            <div class="field">
                <label>Image</label>
                <input type="file" name="image" value="">
            </div>
            <div class="field">
                <label>Name</label>
                <input type="text" name="name" placeholder="add the new element" value="<?= $insertData['name'] ?>">
                <?= isset($errorLabel['name']) ? $errorLabel['name'] : ' ' ?>
            </div>
        </div>
        <div class="two fields">
            <div class="field">
                <label>Brand</label>
                <select class="ui fluid dropdown" name="brand">
                    <option value="<?= $insertData['brand'] ?>">Brand..</option>
                    <?php foreach($brand AS $row): ?>
                        <option value="<?=$row['id']?>"><?= $row['name'] ?></option>
                    <?php endforeach; ?>
                </select>
                <?= isset($errorLabel['brand']) ? $errorLabel['brand'] : ' ' ?>
            </div>
            <div class="field">
                <label>Category</label>
                <select class="ui dropdown" name="category[]" multiple="">
                    <option value=""></option>
                    <?php foreach ($category as $row): ?>
                        <option value="<?=$row['id']?>" <?= ($insertData['category']) ? checkForIdInArray($insertData['category'], $row['id']) : ''?>><?=$row['name']?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>


        <div class="field">
            <label>Text</label>
            <textarea class="tinymce-editor" name="text"></textarea>
        </div>
    <button class="ui button CIYellow" type="submit">Submit</button>
</form>