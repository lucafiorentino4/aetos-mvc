<?php

$mode = $_GET['mode'];
//$data = $this->data;
$data = $this->allCameras;
function cutString($string){
    if(strlen($string) > 50) {
        $addString = substr($string,0,50)."...";
    } else {
        $addString = $string;
    } return $addString;
};
?>


<div class="ui buttons ">
    <a class="ui labeled icon button black" href="<?=URL?>admin">
        <i class="caret left icon"></i>
        Zurück
    </a>
</div>

<h1>Kameras</h1>

<div class="ui clearing container right aligned">
    <a class="ui labeled icon button black" href="<?=URL?>adminCameras/openNewCameraForm?mode=<?=$mode?>">
        <i class="plus icon white"></i>
        Neue Kamera
    </a>
</div>

<?php if(count($data) > 0): ?>
    <table class="ui celled table inverted">
        <thead>
        <tr>
            <?php foreach ($data[0] as $key => $value): ;?>
                <?php if($key == 'text'): ?>
                    <th class="widthTextTable"><?= $key ?></th>
                <?php else: ?>
                    <th><?= $key ?></th>
                <?php endif; ?>
            <?php endforeach; ?>
            <th>actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $row): $categorys = $row['category']; unset($row['category'])?>
            <tr>
                <?php foreach ($row as $key => $value): ?>
                    <?php if($key == 'image'): ?>
                        <td data-label="<?= $key ?>">
                            <?php if($value): ?>
                               <img class="objectDetailThumb" src="<?=URL . $value ?>" alt="">
                            <?php else: ?>
                               <img class="objectDetailThumb" src="<?=URL . IMAGE_PLACEHOLDER_THUMB?>" alt="">
                            <?php endif; ?>
                        </td>
                    <?php else: ?>
                        <?php if($key == 'text'): ?>
                            <td  data-label="<?= $key ?>" >
                                <?= cutString($value) ?>
                            </td>
                        <?php else: ?>
                            <td  data-label="<?= $key ?>">
                                <?= $value ?>
                            </td>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
                <td>
                    <?php foreach ($categorys as $categoryList): ?>
                        &#10003; <?= $categoryList['name'] ?> <br/>
                    <?php endforeach; ?>
                </td>
                <td data-label="Name">
                    <a class="ui icon button black deleteButton" data-name="<?=$row['name']?>" data-href="<?=URL?>adminCameras/deleteCamera/<?=$row['id']?>/?mode=<?=$mode?>"><i class="trash icon white"></i></a>
                    <a class="ui icon button CIYellow" href="<?=URL?>adminCameras/editCamera/<?=$row['id']?>/?mode=<?=$mode?>"><i class="pencil alternate icon white"></i></a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

<div class="ui modal deleteMessage">
    <div class="header CIBlack">Caution</div>
    <div class="content CIBlack">
        <p>Do you want to delete <?=ucfirst($mode)?> <span id="deleteMessageName">XYZ</span></p>
    </div>
    <div class="actions CIBlack">
        <a id="confirmDelete" class="ui approve button CIYellow">Delete</a>
        <a class="ui cancel button CIWhite">Cancel</a>
    </div>
</div>

<script>
    let allDeleteButtons = document.querySelectorAll('.deleteButton');
    let confirmDeleteButton = document.querySelector('#confirmDelete');
    let deleteMessageName = document.querySelector('#deleteMessageName');

    allDeleteButtons.forEach((element) => {
        element.addEventListener('click', function() {
            confirmDeleteButton.href = this.getAttribute('data-href');
            deleteMessageName.innerHTML = this.getAttribute('data-name');
            $('.ui.modal.deleteMessage').modal('show');
        });
    });
</script>
