<?php

$allDroneData = $this->allDroneData;

?>

<div class="ui buttons ">
    <a class="ui labeled icon button black" href="<?=URL?>drones">
        <i class="caret left icon"></i>
        Zurück
    </a>
</div>
<div>
     <div class="ui grid infoBoxObjectADS">
        <div class="ui eight wide computer sixteen wide mobile eight wide tablet aligned centered column infoBoxADS">
            <div class="objectName">
                <div class="nameObjectStyle">
                    <?=$allDroneData['name'] ?>
                </div>
                <div>
                    <?=$allDroneData['brand'] ?>
                </div>
            </div>
            <div class="subtitleCamerasDronesADS">
                Unterstütze Kameras:
            </div>
            <div class=" camerasDroneInfosADS">
                <?php foreach($allDroneData['cameras'] as $droneCamera): ?>
                    <a class="cameraLabelADS" href="<?=URL?>cameras/cameraDetails/<?=$droneCamera['id']?>">
                        <?= $droneCamera['name'] ?>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="ui eight wide computer sixteen wide mobile eight wide tablet aligned centered column ">
            <div class="column objectImage">
                <?php if($allDroneData['image_url']): ?>
                    <img  src="<?=URL . $allDroneData['image_url'] ?>" alt="">
                <?php else: ?>
                    <img  src="<?=URL . IMAGE_PLACEHOLDER?>" alt="">
                <?php endif; ?>
            </div>
            <div class="categoryDroneInfosADS ">
                <?php foreach($allDroneData['category'] as $droneCamera): ?>
                    <div class="categoryLabelDroneInfosADS ">
                        <?= $droneCamera['name'] ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="textFormat">
        <?=$allDroneData['text'] ?>
    </div>
</div>
