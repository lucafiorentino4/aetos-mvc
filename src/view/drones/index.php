<?php
$drones = $this->allDrones;
$search = isset($this->search) ? $this->search : null;
?>
<!-- if the search do not match show, show no match massage -->
<?php if ($search && count($drones) == 0): ?>
<div class="ui grid">
        <img class="ui image large centered paddingTopDown" src="<?=URL?>public/assets/image/drone-not-found.svg" alt="">
        <h3 class="centered row">Wir haben leider keine Ergebnisse für "<?=$search?>" gefunden versuchen Sie es nochmal. </h3>
</div>

<!-- else show search match or all the drone list -->
<?php else: ?>
<h2>Drohnen</h2>
    <div>
        <!-- for each camera detail create a styled div -->
        <?php foreach ($drones as $drone): $objectURL = URL . 'drones/droneDetails/' . $drone['id']; ?>
            <div class="ui grid ObjectBox">
                <div class="ui eight wide computer sixteen wide mobile eight wide tablet column aligned centered objectInfosCentered paddingObjectData">
                    <a class="headerStyleDroneInfos" href="<?= $objectURL ?>">
                        <span><?= $drone['name'] ?></span> <span class="brandStyleObjectInfos"><?= $drone['brand'] ?></span>
                    </a>
                    <div class="camerasDroneInfos">
                        <?php if (!!$drone['cameras'] ): ?>
                            <i class="ui icon camera"></i>
                        <?php else: ?>
                        <?php endif;?>
                        <?php foreach($drone['cameras'] as $value): ?>
                            <span class="cameraLabel">
                                 <a href="<?= $objectURL ?>">
                                    <?= $value['name'] ?> |
                                 </a>
                             </span>
                        <?php endforeach; ?>
                    </div>
                    <div class="description">
                    </div>
                    <div class="labelBoxObjectInfos">
                        <?php foreach($drone['category'] as $value): ?>
                            <span class="labelStyle">
                                <?=$value['name']?>
                            </span> |
                        <?php endforeach; ?>
                    </div>
                    <a href="<?= $objectURL ?>">
                        <div class="ui animated button CIYellow" tabindex="0">
                            <div class="visible content">
                                <i class="info icon"></i> Mehr Erfahren
                            </div>
                            <div class="hidden content">
                                <i class="angle right icon"></i>
                            </div>
                        </div>
                    </a>
                </div>
                    <a class="ui eight wide computer sixteen wide mobile eight wide tablet column aligned centered paddingObjectData centerPic" href="<?= $objectURL ?>">
                        <?php if($drone['image']): ?>
                            <img  class="image-style" src="<?=URL . $drone['image'] ?>" alt="">
                        <?php else: ?>
                            <img class="image-style" src="<?=URL . IMAGE_PLACEHOLDER?>" alt="">
                        <?php endif; ?>
                    </a>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>