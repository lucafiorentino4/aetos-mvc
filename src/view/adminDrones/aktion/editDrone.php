<?php
$mode = $_GET['mode'];

$brand = $this->brand;
$category = $this->category;
$cameras = $this->cameras;
$allDrones = $this->allDrones;


function checkForIdInArray($array, $object_id){
    foreach ($array as $row) {
        if ($row['id'] == $object_id)
            return $result = 'selected';
    }

    return $result = ' ';
}

$actionURL = URL . "adminDrones/updateDrone/" . $allDrones['id'] . "?mode=" . $mode;


?>

<div class="ui buttons ">
    <a class="ui labeled icon button black" href="<?=URL?>adminDrones/showDronesDetail?mode=<?=$mode?>">
        <i class="caret left icon"></i>
        Zurück
    </a>
</div>

<h1>Update von "<?= ucfirst($allDrones['name']) ?>"</h1>

<form class="ui form" action="<?= $actionURL ?>" method="POST" enctype="multipart/form-data">
    <div class="ui grid field">
        <input type="hidden" name="MAX_FILE_SIZE" value="6000000">
        <div class="ui six field wide">
            <img class="editObjectDetailThumb" src="<?= URL . $allDrones['image_url'] ?>" alt="">
            <div class="field">
                <label class="labelColor">Image</label>
                <input type="file" name="image">
            </div>
        </div>
        <div class="ui ten field wide">
            <div class="two fields">
                <div class="field">
                    <label class="labelColor">Name</label>
                    <input type="text" name="name"  value="<?=$allDrones['name']?>">
                </div>
                <div class="field">
                    <label class="labelColor">Brand</label>
                    <select class="ui fluid dropdown" name="brand">
                        <option value="<?=$allDrones['brand_id']?>">Brand..</option>
                        <?php foreach($brand AS $row): ?>
                            <option value="<?=$row['id']?>" ><?= $row['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <label class="labelColor">Cameras</label>
                    <select class="ui dropdown" name="cameras[]" multiple="">
                        <option value=""></option>
                        <?php foreach ($cameras as $row):  ?>
                            <option value="<?=$row['id']?>" <?= checkForIdInArray($allDrones['cameras'], $row['id']); ?>> <?= $row['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="field">
                    <label>Category</label>
                    <select class="ui dropdown" name="category[]" multiple="">
                        <option value=""></option>
                        <?php foreach ($category as $row): ?>
                            <option value="<?=$row['id']?>" <?= checkForIdInArray($allDrones['category'], $row['id']); ?>><?= $row['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="field">
        <label>Text</label>
        <textarea class="tinymce-editor" name="text"><?=$allDrones['text']?></textarea>
    </div>
    <button class="ui button CIYellow" type="submit">Submit</button>
</form>
