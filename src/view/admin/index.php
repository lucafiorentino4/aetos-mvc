<?php
?>

<h1>Back-Office</h1>

<div class="ui link cards aligned centered">
    <div class="card">
        <a class="svgBox" href="<?URL?>adminDrones/showDronesDetail?mode=drones">
            <img class="svgStyling" src="<?=URL?>public/assets/backoffice/drones.svg">
        </a>
        <a class="content" href="<?URL?>adminDrones/showDronesDetail?mode=drones">
            <div class="header">Drohnen</div>
        </a>
        <a class="ui bottom attached button CIYellow" href="<?URL?>adminDrones/showDronesDetail?mode=drones">
            <i class="edit icon"></i>
            Drohnen Anzeigen
        </a>
    </div>
    <div class="card" >
        <a class="svgBox" href="<?URL?>adminCameras/showCamerasDetail?mode=cameras">
            <img class="svgStyling" src="<?=URL?>public/assets/backoffice/cameras.svg">
        </a>
        <a class="content" href="<?URL?>adminCameras/showCamerasDetail?mode=cameras">
            <div class="header">Kameras</div>
        </a>
        <a class="ui bottom attached button CIYellow" href="<?URL?>adminCameras/showCamerasDetail?mode=cameras">
            <i class="edit icon"></i>
            Kameras Anzeigen
        </a>
    </div>

    <div class="card">
        <a class="svgBox" href="<?URL?>admin/showInfosDetail?mode=brands">
            <img class="svgStyling" src="<?=URL?>public/assets/backoffice/brands.svg">
        </a>
        <a class="content" href="<?URL?>admin/showInfosDetail?mode=brands">
            <div class="header">Marken</div>
        </a>
        <a class="ui bottom attached button CIYellow" href="<?URL?>admin/showInfosDetail?mode=brands">
            <i class="edit icon"></i>
            Marken Anzeigen
        </a>
    </div>
    <div class="card">
        <a class="svgBox" href="<?URL?>admin/showInfosDetail?mode=category">
            <img class="svgStyling" src="<?=URL?>public/assets/backoffice/categories.svg">
        </a>
        <a class="content" href="<?URL?>admin/showInfosDetail?mode=category">
            <div class="header">Kategorien</div>
        </a>
        <a class="ui bottom attached button CIYellow" href="<?URL?>admin/showInfosDetail?mode=category">
            <i class="edit icon"></i>
            Kategorien Anzeigen
        </a>
    </div>
</div>