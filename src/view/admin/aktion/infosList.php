<?php
    $mode = $_GET['mode'];
    //$data = $this->data;
    $data = $info = $this->info;
?>

<div class="ui buttons ">
    <a class="ui labeled icon button black" href="<?=URL?>admin">
        <i class="caret left icon"></i>
        Zurück
    </a>
</div>

<h1>
    <?php if($mode === 'brands'): ?>
    Marken
    <?php elseif($mode === 'category'): ?>
    Kategorien
    <?php else: ?>

    <?php endif; ?>
</h1>

<div class="ui clearing container right aligned">
    <a class="ui labeled icon button black" href="<?=URL?>admin/openNewInfoForm?mode=<?=$mode?>">
        <i class="plus icon white"></i>
        Neue
        <?php if($mode === 'brands'): ?>
            Marke
        <?php elseif($mode === 'category'): ?>
            Kategorie
        <?php else: ?>

        <?php endif; ?>
    </a>
</div>

<?php if(count($data) > 0): ?>
    <table class="ui celled table inverted">
        <thead>
        <tr>
            <?php foreach ($data[0] as $key => $value):?>
                <th><?= $key ?></th>
            <?php endforeach; ?>
            <th>actions</th>
        </tr>
        </thead>
        <tbody>
        <?php $i = 0; foreach ($data as $row): ?>
            <tr>
                <?php foreach ($row as $key => $value): ?>
                    <td data-label="Name">
                        <?= $value ?>
                    </td>
                <?php endforeach; ?>
                <td data-label="Name">
                    <a class="ui icon button black deleteButton" data-name="<?=$row['name']?>" data-href="<?=URL?>admin/deleteInfo/<?=$row['id']?>/?mode=<?=$mode?>"><i class="trash icon white"></i></a>
                    <a class="ui icon button CIYellow" href="<?=URL?>admin/editInfos/<?=$row['id']?>/?mode=<?=$mode?>"><i class="pencil alternate icon white"></i></a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

<div class="ui modal deleteMessage">
    <div class="header CIBlack">Caution</div>
    <div class="content CIBlack">
        <p>Do you want to delete <?=ucfirst($mode)?> <span id="deleteMessageName">XYZ</span></p>
    </div>
    <div class="actions CIBlack">
        <a id="confirmDelete" class="ui approve button CIYellow">Delete</a>
        <a class="ui cancel button CIWhite">Cancel</a>
    </div>
</div>

<script>
    let allDeleteButtons = document.querySelectorAll('.deleteButton');
    let confirmDeleteButton = document.querySelector('#confirmDelete');
    let deleteMessageName = document.querySelector('#deleteMessageName');

    allDeleteButtons.forEach((element) => {
        element.addEventListener('click', function() {
            confirmDeleteButton.href = this.getAttribute('data-href');
            deleteMessageName.innerHTML = this.getAttribute('data-name');
            $('.ui.modal.deleteMessage').modal('show');
        });
    });
</script>
