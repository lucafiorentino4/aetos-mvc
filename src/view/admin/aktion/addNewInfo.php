<?php
    $mode = $_GET['mode'];


    $actionURL = URL . "admin/addNewInfosToDB/" . "?mode=" . $mode;
?>

<div class="ui buttons ">
    <a class="ui labeled icon button black" href="<?=URL?>admin/showInfosDetail?mode=<?=$mode?>">
        <i class="caret left icon"></i>
        Zurück
    </a>
</div>

<h2><?=ucfirst($mode)?> neu hinzufügen.</h2>

<form class="ui form"  method="POST" action="<?= $actionURL ?>">
    <div class="ui form">
        <div class="fields">
            <div class="field">
                <label>Name</label>
                <input type="text" name="name" placeholder="add the new element">
            </div>
        </div>
    </div>
    <button class="ui button CIYellow" type="submit">Submit</button>
</form>