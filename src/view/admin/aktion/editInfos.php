<?php
$mode = $_GET['mode'];

$element = $this->row;
$brand = $this->brand;


$actionURL = URL . "admin/updateInfos/" . $element['id'] . "?mode=" . $mode;

?>

<div class="ui buttons ">
    <a class="ui labeled icon button black" href="<?=URL?>admin/showInfosDetail?mode=<?=$mode?>">
        <i class="caret left icon"></i>
        Back
    </a>
</div>

<h1><?= ucfirst($element['name']) ?> </h1>

<form class="ui form" action="<?= $actionURL ?>" method="POST" enctype="multipart/form-data">
    <div class="fields">
        <div class="field">
            <label>Name</label>
            <input type="text" name="name"  value="<?=$element['name']?>">
        </div>
    </div>
    <button class="ui button CIYellow" type="submit">Submit</button>
</form>