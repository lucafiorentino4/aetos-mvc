<?php

$user = Session::get('user');

$error = isset($this->error) ? $this->error : null;

$errorLabel = array();

if ($error){
    foreach ($error as $key => $value){
        $ucFirstKey = ucfirst($key);
            $errorLabel[$key] = "<div class=\"ui pointing red basic label\"><i class=\"exclamation triangle icon\"></i> $ucFirstKey: $value</div>";
        }
}

?>
<div class="ui grid">
    <div class="centered row">
        <h1 class="header">Profil Bearbeitung</h1>
    </div>
    <form class="ui eight wide computer sixteen wide mobile sixteen wide tablet form column centered" method="post" action="<?=URL?>userProfile/aktualizeData">
        <div class="ui grid">
            <div class="ui eight wide computer sixteen wide mobile eight wide tablet form column">
                <div class="ui field">
                    <label>Vorname</label>
                    <input type="text" name="firstname" value="<?= $user['firstname'] ?>">
                    <?= isset($errorLabel['firstname']) ? $errorLabel['firstname'] : ' ' ?>
                </div>
                <div class="field">
                    <label>Nachname</label>
                    <input type="text" name="lastname" value="<?= $user['lastname'] ?>">
                    <?= isset($errorLabel['lastname']) ? $errorLabel['lastname'] : ' ' ?>
                </div>
            </div>
            <div class="ui eight wide computer sixteen wide mobile eight wide tablet form column">
                <div class="field">
                    <label>Aktuelle E-mail-Adresse: </label>
                    <div class="emailField CIYellow"><?= $user['email'] ?></div>
                </div>
                <div class="field">
                    <label>Neue E-Mail-Adresse: </label>
                    <input type="text" name="email" value="<?= $user['email'] ?>">
                    <?= isset($errorLabel['email']) ? $errorLabel['email'] : '' ?>
                </div>
            </div>
            <div class="ui eight wide computer sixteen wide mobile eight wide tablet form column">
                <div class="field">
                    <label>
                        Zum Bestätigen bitte Password einfügen:
                    </label>
                </div>
                <div class="ui eight wide computer sixteen wide mobile eight wide tablet form column">
                    <div class="field">
                        <input type="password" name="password" value="">
                        <?= isset($errorLabel['password']) ? $errorLabel['password'] : '' ?>
                    </div>
                    <div class="column">
                        <button class="ui button CIYellow" type="submit">
                            Submit
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>




