<?php

    $user = Session::get('user');

?>
<div class="ui grid marginUp">
    <div class="ui eight wide computer sixteen wide mobile sixteen wide tablet centered column">
        <h1 class="ui centered header">Hallo <?= $user['firstname'] ?></h1>
        <div class="ui centered card">
            <div class="content">
                <a class="header">
                    <i class="user icon"></i>
                    <?= $user['firstname']?>, <?= $user['lastname']?>
                </a>
            </div>
            <div class="image">
                <img src="<?= URL . USER_IMAGE_PLACEHOLDER ?>" />
            </div>
            <div class="extra content">
                <div>
                    <i class="envelope outline icon"></i>
                    <b class="blue">E-Mail:</b>
                </div>
                <div>
                    <?= $user['email'] ?>
                </div>
            </div>
            <div class="extra content">
                <div class="extra content">
                    <div class="meta date"><b>Beigetreten:</b></div>
                    <div class="meta date"><?= $user['created'] ?></div>
                </div>
                <div class="extra content">
                    <div class="meta date"><b>Bearbeitet am:</b></div>
                    <div class="meta date"><?= $user['updated'] ?></div>
                </div>
            </div>
            <a class="ui bottom attached button CIYellow" href="<?=URL?>userProfile/changeUserData">
                <i class="edit icon"></i>
                Profil Bearbeiten
            </a>
        </div>
    </div>
</div>

