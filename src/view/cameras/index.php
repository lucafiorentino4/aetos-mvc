<?php
//data from database
    $cameras = $this->allCameras;
    $search = isset($this->search) ? $this->search : null;
?>

<!-- if the search do not match show, show no match massage -->
<?php if ($search && count($cameras) == 0): ?>
    <div class="ui grid">
        <img class="ui image medium centered paddingTopDown" src="<?=URL?>public/assets/image/camera-not-found.svg" alt="">
        <h3 class="centered row">Wir haben leider keine Ergebnisse für "<?=$search?>" gefunden versuchen Sie es nochmal. </h3>
    </div>

<!-- else show search match or all the drone list -->
<?php else: ?>
<h2>Kameras</h2>
<div>
    <!-- for each camera detail create a styled div -->
    <?php foreach ($cameras as $camera):  $objectURL = URL . 'cameras/cameraDetails/' . $camera['id']; ?>
        <div class="ui grid ObjectBox">
            <div class="ui eight wide computer sixteen wide mobile eight wide tablet column aligned centered objectInfosCentered paddingObjectData">
                <a class="headerStyleDroneInfos" href="<?= $objectURL ?>">
                    <span><?= $camera['name'] ?></span> <span class="brandStyleObjectInfos"><?= $camera['brand'] ?></span>
                </a>
                <div class="labelBoxObjectInfos">
                    <?php foreach($camera['category'] as $value): ?>
                        <span class="labelStyle">
                            <?= $value['name'] ?>
                        </span> |
                    <?php endforeach; ?>
                </div>
                <a href="<?= $objectURL ?>">
                    <div class="ui animated button CIYellow" tabindex="0">
                        <div class="visible content">
                            <i class="info icon"></i> Mehr Erfahren
                        </div>
                        <div class="hidden content">
                            <i class="angle right icon"></i>
                        </div>
                    </div>
                </a>
            </div>
            <a class="ui eight wide computer sixteen wide mobile eight wide tablet column aligned centered paddingObjectData centerPic" href="<?= $objectURL ?>">
                <?php if($camera['image']): ?>
                    <img  class="image-style" src="<?=URL . $camera['image'] ?>" alt="">
                <?php else: ?>
                    <img  class="image-style" src="<?=URL . IMAGE_PLACEHOLDER?>" alt="">
                <?php endif; ?>
            </a>

        </div>
    <?php endforeach; ?>
</div>
<?php endif; ?>