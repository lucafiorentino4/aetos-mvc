<?php
$allCameraData = $this->allCameraData;
?>

<div class="ui buttons ">
    <a class="ui labeled icon button black" href="<?=URL?>cameras">
        <i class="caret left icon"></i>
       Zurück
    </a>
</div>

<div>
    <div class="ui grid infoBoxObjectADS">
        <div class="ui eight wide computer sixteen wide mobile eight wide tablet aligned centered column infoBoxADS">
            <div class="objectName">
                <div class="nameObjectStyle">
                    <?=$allCameraData['name'] ?>
                </div>
                <div>
                    <?=$allCameraData['brand'] ?>
                </div>
            </div>
            <div class="categoryDroneInfosADS ">
                <?php foreach($allCameraData['category'] as $cameraCategory): ?>
                    <div class="categoryLabelDroneInfosADS " >
                        <?= $cameraCategory['name'] ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="ui eight wide computer sixteen wide mobile eight wide tablet aligned centered column ">
            <div class="column objectImage">
                <?php if($allCameraData['image_url']): ?>
                    <img  src="<?=URL . $allCameraData['image_url'] ?>" alt="Camera <?= $allCameraData['name'] ?> von der Marke: <?= $allCameraData['brand'] ?> ">
                <?php else: ?>
                    <img  src="<?=URL . IMAGE_PLACEHOLDER?>" alt="Camera <?= $allCameraData['name'] ?> von der Marke: <?= $allCameraData['brand'] ?> ">
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="textFormat">
        <?=$allCameraData['text'] ?>
    </div>
</div>