<?php
$teaserSet = $this->teaserSet;
?>
<div class="ui field">
    <a class="ui row paddingTopDown image buttonOnTeaserSettings" href="<?=URL?><?=$teaserSet['a_link']?>">
        <img src="<?= URL . $teaserSet['a_teaser_url']?>" alt="<?=$teaserSet['a_alt_tag']?>" title="<?=$teaserSet['a_title_tag']?>"/>
        <div class="buttonOnTeaser">
            <div class="ui animated button CIYellow" tabindex="0">
                <div class="visible content">
                    <i class="info icon"></i> Mehr Erfahren
                </div>
                <div class="hidden content">
                    <i class="angle right icon"></i>
                </div>
            </div>
        </div>
    </a>
    <div class="ui grid">
        <div class="ui eight wide computer eight wide tablet sixteen wide mobile centered column flexTeaser">
            <a class="ui column" href="<?=URL?><?=$teaserSet['b_link']?>">
                <img clasS="ui centered image" src="<?= URL . $teaserSet['b_teaser_url']?>" alt="<?=$teaserSet['b_alt_tag']?>" title="<?=$teaserSet['b_title_tag']?>"/>
            </a>
            <a class="ui column" href="<?=URL?><?=$teaserSet['c_link']?>">
                <img class="ui centered image" src="<?= URL . $teaserSet['c_teaser_url']?>" alt="<?=$teaserSet['c_alt_tag']?>" title="<?=$teaserSet['c_title_tag']?>"/>
            </a>
        </div>
        <a class="ui eight wide computer eight wide tablet sixteen wide mobile column buttonOnTeaserSettings" href="<?=URL?><?=$teaserSet['d_link']?>">
           <img class="ui centered image" src="<?= URL . $teaserSet['d_teaser_url']?>" alt="<?=$teaserSet['d_alt_tag']?>" title="<?=$teaserSet['d_title_tag']?>">
           <div class="buttonOnTeaser">
               <div class="ui animated button CIYellow" tabindex="0">
                   <div class="visible content">
                       <i class="info icon"></i> Mehr Erfahren
                   </div>
                   <div class="hidden content">
                       <i class="angle right icon"></i>
                   </div>
               </div>
           </div>
        </a>
    </div>
</div>
